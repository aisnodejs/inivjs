'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ServerServiceProvider Class
 */
class ServerServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }

    /**
   * Register auth provider under `Iniv/Auth` namespace
   *
   * @method _registerAuth
   *
   * @return {void}
   *
   * @private
   */
  _registerAuth () {
    this._iniv._ioc.register('Iniv/Auth', () => {return require('./Auth')})
  }

  /**
   * Register auth manager under `Iniv/Auth` namespace
   *
   * @method _registerAuthManager
   *
   * @return {void}
   *
   * @private
   */
  _registerAuthManager () {
    this._iniv._ioc.register('Iniv/Auth/Manager', () => {require('./Auth/Manager')})
  }

  /**
   * Register authinit middleware under `Iniv/Middleware/AuthInit`
   * namespace.
   *
   * @method _registerAuthInitMiddleware
   *
   * @return {void}
   */
  _registerAuthInitMiddleware () {
    this._iniv._ioc.bind('Iniv/Middleware/AuthInit', () => {
      const AuthInit = require('./Middleware/AuthInit')
      return new AuthInit(load('Iniv/Config'))
    })
  }

  /**
   * Register auth middleware under `Iniv/Middleware/Auth` namespace.
   *
   * @method _registerAuthMiddleware
   *
   * @return {void}
   *
   * @private
   */
  _registerAuthMiddleware () {
    this._iniv._ioc.bind('Iniv/Middleware/Auth', () => {
      const Auth = require('./Middleware/Auth')
      return new Auth(load('Iniv/Config'))
    })
  }

  /**
   * Register the vow trait to bind session client
   * under `Iniv/Traits/Session` namespace.
   *
   * @method _registerVowTrait
   *
   * @return {void}
   */
  _registerVowTrait () {
    this._iniv._ioc.bind('Iniv/Traits/Auth', () => {
      const Config = load('Iniv/Config')
      return ({ Request, traits }) => {
        const sessionIndex = _.findIndex(traits, (trait) => trait.action === 'Session/Client')
        const authIndex = _.findIndex(traits, (trait) => trait.action === 'Auth/Client')

        /**
         * The auth/client should be registered before the `session/client` trait. So that
         * session set by auth is sent as cookies.
         */
        if (sessionIndex > -1 && sessionIndex < authIndex) {
          throw new Error(WRONG_ORDER_MESSAGE)
        }
        require('./VowBindings/Request')(Request, Config)
      }
    })
    // this._iniv._ioc.alias('Iniv/Traits/Auth', 'Auth/Client')
  }

  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  async register () {
    this._registerAuth()
    this._registerAuthManager()
    this._registerAuthInitMiddleware()
    this._registerAuthMiddleware()
    this._registerVowTrait()
    return
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  async boot () {
    const HttpContext = this._iniv._ioc.load('Iniv/HttpContext')
    const WsContext = this._iniv._ioc.load('Iniv/WsContext')
    const Auth = this._iniv._ioc.load('Iniv/Auth')
    const Config = this._iniv._ioc.load('Iniv/Config')
    HttpContext.getter('auth', function () {
      return new Auth({ request: this.request, response: this.response, session: this.session }, Config)
    }, true)

    WsContext.getter('auth', function () {
      return new Auth({ request: this.request, response: this.response, session: this.session }, Config)
    }, true)

    /**
     * Adding `loggedIn` tag to the view, only when view
     * provider is registered
     */
    try {
      const View = this._iniv._ioc.load('Iniv/View')
      require('./ViewBindings')(View)
    } catch (error) {}
    return true
  }
}
module.exports = ServerServiceProvider