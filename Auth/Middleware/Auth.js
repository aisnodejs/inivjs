'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

const _ = require('lodash')

class Auth {
  constructor (Config) {
    const authenticator = Config.get('auth.authenticator')
    this.scheme = Config.get(`auth.${authenticator}.scheme`, null)
  }

  /**
   * Authenticate the user using one of the defined
   * schemes or the default scheme
   *
   * @method handle
   *
   * @param  {Object}   options.auth
   * @param  {Function} next
   *
   * @return {void}
   */
  async handle ({ auth, view }, next, schemes) {
    let lastError = null
    let authenticatedScheme = null

    schemes = _.castArray(schemes || this.scheme)
    Logger.silly('attempting to authenticate via %j scheme(s)', schemes)

    /**
     * Loop over all the defined schemes and wait until use is logged
     * via anyone
     */
    for (const scheme of schemes) {
      try {
        await auth.authenticator(scheme).check()
        Logger.silly('authenticated using %s scheme', scheme)
        authenticatedScheme = scheme
        lastError = null
        break
      } catch (error) {
        Logger.silly('authentication failed using %s scheme', scheme)
        lastError = error
      }
    }

    /**
     * If there is an error from all the schemes
     * then throw it back
     */
    if (lastError) {
      throw lastError
    }

    /**
     * If user got logged then set the `current` property
     * on auth, which is reference to the scheme via
     * which user got authenticated.
     */
    if (authenticatedScheme) {
      /**
       * If logged in scheme is same as the default scheme, the reference
       * the actual authenticator instance, otherwise create a new
       * one for the scheme via which user got authenticated
       */
      auth.current = authenticatedScheme === this.scheme
      ? auth.authenticatorInstance
      : auth.authenticator(authenticatedScheme)
    }

    /**
     * Sharing user with the view
     */
    if (view && typeof (view.share) === 'function') {
      view.share({
        auth: {
          user: auth.current.user
        }
      })
    }

    await next()
  }
   /**
   * Authenticate the user using one of the defined
   * schemes or the default scheme
   *
   * @method handle
   *
   * @param  {Object}   options.auth
   * @param  {Function} next
   *
   * @return {void}
   */
  async handleWs ({ auth, request, socket }, next, schemes) {
    let lastError = null
    let authenticatedScheme = null
    
    schemes = _.castArray(schemes || this.scheme)
    Logger.silly('attempting to authenticate via %j scheme(s)', schemes)

    /**
     * Loop over all the defined schemes and wait until use is logged
     * via anyone
     */
    for (const scheme of schemes) {
      try {
        await auth.authenticator(scheme).check()
        Logger.silly('authenticated using %s scheme', scheme)
        authenticatedScheme = scheme
        lastError = null
        break
      } catch (error) {
        Logger.silly('authentication failed using %s scheme', scheme)
        lastError = error
      }
    }

    /**
     * If there is an error from all the schemes
     * then throw it back
     */
    if (lastError) {
      throw lastError
    }

    /**
     * If user got logged then set the `current` property
     * on auth, which is reference to the scheme via
     * which user got authenticated.
     */
    if (authenticatedScheme) {
      /**
       * If logged in scheme is same as the default scheme, the reference
       * the actual authenticator instance, otherwise create a new
       * one for the scheme via which user got authenticated
       */
      auth.current = authenticatedScheme === this.scheme
      ? auth.authenticatorInstance
      : auth.authenticator(authenticatedScheme)
    }

    request.user = auth.current.user
    socket.user = request.user

    await next()
  }
}

module.exports = Auth
