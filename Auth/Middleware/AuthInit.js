'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

class AuthInit {
  constructor (Config) {
    const authenticator = Config.get('auth.authenticator')
    this.scheme = Config.get(`auth.${authenticator}.scheme`, null)
  }

  /**
   * Check the user login status if scheme in use
   * in session. Since it will make the `user`
   * instance available on each request.
   *
   * @method handle
   *
   * @param  {Object}   options.auth
   * @param  {Function} next
   *
   * @return {void}
   */
  async handle ({ auth, view }, next) {
    if (this.scheme === 'session') {
      await auth.loginIfCan()
    }
    
    /**
     * Sharing user with the view
     */
    if (view && typeof (view.share) === 'function') {
      view.share({
        auth: {
          user: auth.user
        }
      })
    }
    await next()
  }
  /**
   * authenticates a web socket request using number of defined
   * authenticators.
   *
   * @param  {Object}   socket
   * @param  {Object}   request
   * @param  {Function} next
   *
   * @public
   */
  async handleWs ({socket, request, auth}, next) {
    if (this.scheme === 'session') {
      await auth.loginIfCan()
    }
    
    try{
      request.user = await auth.getUser()
    }catch(e){
      request.user = null
    }
    socket.user = request.user
    await next()
  }
}

module.exports = AuthInit
