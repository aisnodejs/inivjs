'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

module.exports = {
  session: require('./Session'),
  basic: require('./BasicAuth'),
  jwt: require('./Jwt'),
  api: require('./Api')
}
