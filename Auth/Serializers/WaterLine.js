'use strict'

/**
 * WaterLine serializers uses lucid model to validate
 * and fetch user details.
 *
 * @class WaterLineSerializer
 * @constructor
 */
class WaterLineSerializer {
  constructor (Hash) {
    this.Hash = Hash
    this._config = null
    this._Model = null
    this._queryCallback = null
  }

  /* istanbul ignore next */
  /**
   * Dependencies to be injected by Ioc container
   *
   * @attribute inject
   *
   * @return {Array}
   */
  static get inject () {
    return ['Iniv/Hash']
  }

  /**
   * Returns an instance of the model query
   *
   * @method _getQuery
   *
   * @return {Object}
   *
   * @private
   */
  _getQuery () {
    const query = this._Model.findOne()
    if (typeof (this._queryCallback) === 'function') {
      this._queryCallback(query)
    this._queryCallback = null
    }
    return query
  }

  /**
   * Setup config on the serializer instance. It
   * is import and needs to be done as the
   * first step before using the serializer.
   *
   * @method setConfig
   *
   * @param  {Object}  config
   */
  setConfig (config) {
    this._config = config
    this._Model = load(this._config.model)
  }

  /**
   * Returns the primary key for the
   * model. It is used to set the
   * session key
   *
   * @method primaryKey
   *
   * @return {String}
   */
  get primaryKey () {
    return this._Model.primaryKey
  }

  /**
   * Add runtime constraints to the query builder. It
   * is helpful when auth has extra constraints too
   *
   * @method query
   *
   * @param  {Function} callback
   *
   * @chainable
   */
  query (callback) {
    this._queryCallback = callback
    return this
  }

  /**
   * Returns a user instance using the primary
   * key
   *
   * @method findById
   *
   * @param  {Number|String} id
   *
   * @return {User|Null}  The model instance or `null`
   */
  async findById (id) {
    Logger.silly('finding user with primary key as %s', id)
    return this._getQuery().where({[this.primaryKey]: id})
  }

  /**
   * Finds a user using the uid field
   *
   * @method findByUid
   *
   * @param  {String}  uid
   *
   * @return {Model|Null} The model instance or `null`
   */
  async findByUid (uid) {
    Logger.silly('finding user with %s as %s', this._config.uid, uid)
    return this._getQuery().where({[this._config.uid]: uid})
  }

  /**
   * Validates the password field on the user model instance
   *
   * @method validateCredentails
   *
   * @param  {Model}            user
   * @param  {String}            password
   *
   * @return {Boolean}
   */
  async validateCredentails (user, password) {
    if (!user || !user[this._config.password]) {
      return false
    }
    return this.Hash.verify(password, user[this._config.password])
  }

  /**
   * Finds a user with token
   *
   * @method findByToken
   *
   * @param  {String}    token
   * @param  {String}    type
   *
   * @return {Object|Null}
   */
  async findByToken (token, type) {
    Logger.silly('finding user for %s token', token)
    let tokenData = await this._Model.tokens().findOne({ token, type, is_revoked: false }).populate('user')
    return tokenData.user
  }

  /**
   * Save token for a user. Tokens are usually secondary
   * way to login a user when their primary login is
   * expired
   *
   * @method saveToken
   *
   * @param  {Object}  user
   * @param  {String}  token
   * @param  {String}  type
   *
   * @return {void}
   */
  async saveToken (user, token, type) {
    const tokenInstance = {}
    tokenInstance.user = user.id
    tokenInstance.token = token
    tokenInstance.type = type
    tokenInstance.is_revoked = false
    Logger.silly('saving token for %s user with %j payload', user.id, tokenInstance)
    await this._Model.tokens().create(tokenInstance).fetch()
  }

  /**
   * Revoke token(s) or all tokens for a given user
   *
   * @method revokeTokens
   *
   * @param  {Object}           user
   * @param  {Array|String}     [tokens = null]
   * @param  {Boolean}          [inverse = false]
   *
   * @return {Number}           Number of impacted rows
   */
  async revokeTokens (user, tokens = null, inverse = false) {
    const query = user.tokens()
    if (tokens) {
      tokens = tokens instanceof Array === true ? tokens : [tokens]
      inverse ? query.whereNotIn('token', tokens) : query.whereIn('token', tokens)
      Logger.silly('revoking %j tokens for %s user', tokens, user.id)
    } else {
      Logger.silly('revoking all tokens for %s user', user.id)
    }
    return query.update({ is_revoked: true })
  }

  /**
   * Delete token(s) or all tokens for a given user
   *
   * @method deleteTokens
   *
   * @param  {Object}           user
   * @param  {Array|String}     [tokens = null]
   * @param  {Boolean}          [inverse = false]
   *
   * @return {Number}           Number of impacted rows
   */
  async deleteTokens (user, tokens = null, inverse = false) {
    const query = this._Model.tokens().destroy({user:user.id})
    if (tokens) {
      tokens = tokens instanceof Array === true ? tokens : [tokens]
      inverse ? query.whereNotIn('token', tokens) : query.where('token', tokens)
      Logger.silly('deleting %j tokens for %s user', tokens, user.id)
    } else {
      Logger.silly('deleting all tokens for %s user', user.id)
    }
    return await query.fetch()
  }

  /**
   * Returns all non-revoked list of tokens for a given user.
   *
   * @method listTokens
   * @async
   *
   * @param  {Object}   user
   * @param  {String}   type
   *
   * @return {Object}
   */
  async listTokens (user, type) {
    return user.tokens().where({ type, is_revoked: false }).fetch()
  }

  /**
   * A fake instance of serializer with empty set
   * of array
   *
   * @method fakeResult
   *
   * @return {Object}
   */
  fakeResult () {
    return new this._Model.Serializer([])
  }
}

module.exports = WaterLineSerializer
