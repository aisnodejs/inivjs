'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

const LoggedInFactory = require('./LoggedIn')

module.exports = function (View) {
  const LoggedIn = LoggedInFactory(View.engine.BaseTag)
  View.tag(new LoggedIn())
}
