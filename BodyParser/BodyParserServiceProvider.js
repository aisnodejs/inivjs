'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * BodyParserServiceProvider Class
 */
class BodyParserServiceProvider {
    constructor(iniv) {
        this._iniv = iniv
    }
    /**
  * Defines the request macro to get an instance of
  * file.
  *
  * @method _defineRequestMacro
  *
  * @return {void}
  *
  * @private
  */
    _defineRequestMacro() {
        let self = this
        const Request = self._iniv._ioc.load('Iniv/Request')

        /**
         * Request macro to access a file from the uploaded
         * files.
         */
        Request.macro('file', function (name, options = {}) {
            const file = self._iniv._ioc.load('Iniv/Helper')._.get(this._files, name)
            /**
             * Return null when there is no file
             */
            if (!file) {
                return null
            }

            /**
             * Return file when it's a single file
             */
            if (!self._iniv._ioc.load('Iniv/Helper')._.isArray(file)) {
                return file.setOptions(options)
            }

            /**
             * Set options to each file when it's an
             * array of files and instead return
             * the file jar
             */
            file.forEach((eachFile) => {
                eachFile.setOptions(options)
            })
            let FileJar = require('./FileJar')
            return new FileJar(file)
        })
    }

    async register() {
        let self = this
        /*
		Register the http server to the system
		*/
        self._iniv._ioc.register('Iniv/Middleware/BodyParser', () => {
            let Config = self._iniv._ioc.load('Iniv/Config');
            return new (require('./Middleware'))(Config);
        }).aliase('BodyParser')
        return
    }
    async boot() {
        this._defineRequestMacro()
        return true
    }
}
module.exports = BodyParserServiceProvider