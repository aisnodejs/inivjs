'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Config Class
 */


class Config {
	constructor(Helper) {
		this.appPath = Helper.configPath;
		this.Helper = Helper;
		this.key = ''
		this.register();
	}
	get(key, defaultValue) {
		let self = this
		const value = self.Helper._.get(this._config, key, defaultValue)
		if (typeof (value) === 'string' && value.startsWith('self::')) {
			return this.get(value.replace('self::', ''))
		}
		return value
	}

	async register() {
		let self = this
		this._config = this.Helper.requireAll({
			dirname: this.appPath,
			filter: /(.*)\.js$/
		})
	}


	set(key, value) {
		if (typeof key != 'string') {
			throw new Error('Please provide string.')
			return;
		}
		var segments = key.split('.');
		if (this._configurations.has(segments[0])) {
			let key = this._configurations.get(segments[0]);
			return this.getInerConfig(key, segments, 1)
		}
	}
	merge(key, defaultValues, customizer) {
		const value = this.get(key, {})
		return this.Helper._.mergeWith(defaultValues, value, customizer)
	}
}

module.exports = Config