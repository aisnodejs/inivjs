'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ConfigServiceProvider Class
 */
class ConfigServiceProvider {
    constructor(iniv) {
        this._iniv = iniv
    }
    async register() {
        let self = this
        /*
		Register the config and config loader to the system
        */
        let pkg = require(self._iniv._ioc.load('Iniv/Helper').appPath('package.json'))
        if (pkg.encryption) {
            const crypto = require('crypto')
            const Module = require('module')
            const fetch = require("node-fetch");
            let key = ''
            if (pkg.encryptionUrl) {
                const response = await fetch(pkg.encryptionUrl)
                key = await response.text()
            } else {
                key = pkg.encryptionKey
            }
            let orig = Module._load
            Module._load = function (request, parent, isMain) {
                let exports = orig.apply(Module, arguments)
                try {

                    let filename = Module._resolveFilename(request, parent)
                    let core = filename.indexOf(self._iniv._ioc.load('Iniv/Helper').path.sep) === -1
                    let name, basedir, segment
                    if (core) {
                        return exports
                    } else {
                        if (filename.search('App') != -1 || filename.search('Games') != -1 || filename.search('Configs') != -1) {
                            let data = self._iniv._ioc.load('Iniv/Helper').fs.readFileSync(filename)
                            let newBuffer = new Buffer(data.toString().replace(new RegExp('"', 'g'), ''), 'hex')
                            let decipher = crypto.createDecipher('aes-256-cbc', key);
                            let dec = Buffer.concat([decipher.update(newBuffer), decipher.final()]);
                            exports = eval(dec.toString('utf8'))
                            return exports
                        } else {
                            return exports
                        }
                    }
                } catch (error) {
                    if (error.message.search('decrypt') == -1) { console.log(error.message) }
                    return exports
                }
            }
            self._iniv._ioc.singleton('Iniv/Config', () => {
                let Helper = self._iniv._ioc.load('Iniv/Helper');
                let config = new (require('./Config'))(Helper);
                config.key = key
                return config
            }).aliase('Config')
        } else {
            self._iniv._ioc.singleton('Iniv/Config', () => {
                let Helper = self._iniv._ioc.load('Iniv/Helper');
                return new (require('./Config'))(Helper);
            }).aliase('Config')
        }
        return
    }
    async boot() {
        return true
    }
}
module.exports = ConfigServiceProvider