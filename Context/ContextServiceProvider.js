'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ServerServiceProvider Class
 */
class ServerServiceProvider {
    constructor(iniv) {
        this._iniv = iniv
    }
    async register() {
        let self = this
        /*
		Register the http server to the system
		*/
        self._iniv._ioc.singleton('Iniv/HttpContext', () => {
            return require('./HttpContext');
        }).aliase('HttpContext')
        self._iniv._ioc.singleton('Iniv/WsContext', () => {
            return require('./WsContext');
        }).aliase('WsContext')
        return
    }
    async boot() {
        let self = this
        this._iniv._ioc.load('Iniv/HttpContext').getter('request', function () {
            return new (self._iniv._ioc.load('Iniv/Request'))(this.req, this.res, self._iniv._ioc.load('Iniv/Config'))
        }, true)

        this._iniv._ioc.load('Iniv/HttpContext').getter('response', function () {
            return new (self._iniv._ioc.load('Iniv/Response'))(this.req, this.res, self._iniv._ioc.load('Iniv/Config'))
        }, true)

        // socket
        
        this._iniv._ioc.load('Iniv/WsContext').getter('request', function () {
            return new (self._iniv._ioc.load('Iniv/Request'))(this.req, this.res, self._iniv._ioc.load('Iniv/Config'))
        }, true)
        
        this._iniv._ioc.load('Iniv/WsContext').getter('socket', function () {
            return new (self._iniv._ioc.load('Iniv/Ws/Socket'))(this.io, this.sock, self._iniv._ioc.load('Iniv/Config'))
        }, true)

        this._iniv._ioc.load('Iniv/WsContext').getter('response', function () {
            return new (self._iniv._ioc.load('Iniv/Ws/Response'))(this.req, this.res, self._iniv._ioc.load('Iniv/Config'))
        }, true)
        return true
    }
}
module.exports = ServerServiceProvider