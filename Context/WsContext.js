'use strict'



const Macroable = require('macroable')

/**
 * An instance of this class is passed to all route handlers
 * and middleware. Also different part of applications
 * can bind getters to this class.
 *
 *
 * @class Context
 * @constructor
 *
 * @example
 * ```js
 * const Context = use('Context')
 *
 * Context.getter('view', function () {
 *   return new View()
 * }, true)
 *
 * // The last option `true` means the getter is singleton.
 * ```
 */
class WsContext extends Macroable {
  constructor (io, sock, req, res) {
    super()
    this._io = io
    /**
     * Node.js ws server socket object
     *
     * @attribute req
     *
     * @type {Object}
     */
    this.sock = sock
    /**
     * Node.js ws server req object
     *
     * @attribute req
     *
     * @type {Object}
     */
    this.req = req

    /**
     * Node.js ws server res object
     *
     * @attribute res
     *
     * @type {Object}
     */
    this.res = res

    this.constructor._readyFns
      .filter((fn) => typeof (fn) === 'function')
      .forEach((fn) => fn(this))
  }
  get io (){
    return this._io
  }
  /**
   * Hydrate the context constructor
   *
   * @method hydrate
   *
   * @return {void}
   */
  static hydrate () {
    super.hydrate()
    this._readyFns = []
  }

  /**
   * Define onReady callbacks to be executed
   * once the request context is instantiated
   *
   * @method onReady
   *
   * @param  {Function} fn
   *
   * @chainable
   */
  static onReady (fn) {
    this._readyFns.push(fn)
    return this
  }
}

/**
 * Defining _macros and _getters property
 * for Macroable class
 *
 * @type {Object}
 */
WsContext._macros = {}
WsContext._getters = {}
WsContext._readyFns = []

module.exports = WsContext