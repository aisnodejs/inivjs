class DatabaseServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
        /*
		Register the http model to the system
		*/
		self._iniv._ioc.singleton('Iniv/Database', () => {
            const Config = self._iniv._ioc.load('Iniv/Config')
            const Helper = self._iniv._ioc.load('Iniv/Helper')
			return new (require('./Manager'))(Helper, Config, self._iniv._ioc);
        }).aliase('DB')
        return
    }
    async boot () {
        let self = this
        self._iniv._ioc.load('Iniv/Database')
        return true
    }
}
module.exports = DatabaseServiceProvider