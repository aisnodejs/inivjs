const Adapters = require('./Orm/Adapters')
const Builder = require('./Orm/Adapters/Builder')
class Manager {
    constructor(Helper, Config, Ioc, config = null){
        this.Helper = Helper
        this.Config = Config
        this.Ioc = Ioc
        /**
         * List of all connection available
         */
        this._connections = {}
        /**
         * Default connection for all query
         */
        this._connection = null
        /**
         * All adapters that are suported by this
         */
        this._adapters = null
        this._adapter = null
        this._builder = null
        /**
         * Setup connection for all query
         */
        this._setConfigs(Config.get('database'))
    }

    _setConfigs(config){
        if(config.hasOwnProperty('datastores')){
            this._connections = config.datastores
            if(config.hasOwnProperty('datastore')){
                this._config = config
                this._initiatDefaultConnection(config.datastore)
            }else{
                throw new Error('NO_DATABASE_CONNECTION')
            }
        }else{
            throw new Error('NO_DATABASE_CONNECTIONS')
        }
    }    
    _initiatDefaultConnection(connection){
        this._adapters = new Adapters(this._config)
        this._connection = this._connections[connection]
        if(this._connection.hasOwnProperty('adapter')){
            this._adapter = this._adapters.getAdapter(connection)
            this._adapter = new this._adapter(this._connection)
            this._adapter.connect()
        }else{
            throw new Error('NO_DATABASE_ADAPTER_ATTRIBUTE')
        }
    }
      

    table (tableName){
        return new Builder({},this._adapter, tableName)
    }

    adapter(name){
        return this._adapter
    }
    getObjectId(id){
        const ObjectID = require('mongodb').ObjectID
        return new ObjectID(id)
    }
}

module.exports = Manager