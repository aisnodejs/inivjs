module.exports  = class Builder {
  constructor(options, adapter, model) {
    this._options = options
    this._adapter = adapter
    this._options.model = model
  }

  limit(limit) {
    this._options.limit = limit
    return this
  }

  select(...select) {
    this._options.select = select
    return this
  }

  where(where) {
    this._options.where = where
    return this
  }

  first() {
    this._options.limit = 1
    return this.get()
  }

  get() {
    return this._adapter.select(this._options)
  }
  
  all() {
    return this._adapter.select(this._options)
  }
  create(rows = []){
    this._options.data = rows
    return this._adapter.create(this._options)
  }

  update(row = {}){
    this._options.data = row
    return this._adapter.update(this._options)
  }
}
