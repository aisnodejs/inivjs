const MongoClient = require('mongodb').MongoClient

class Connecttion {
  constructor(config) {
    this._config = config
    this.connection = null
  }
  async connect() {
    let self = this
    let url = ''
    if (this._config.password) {
      url = `mongodb://${this._config.user}:${this._config.password}@${this._config.host}:${this._config.port?this._config.port:27017}/${this._config.database}`
    } else {
      url = `mongodb://${this._config.host}:${this._config.port?this._config.port:27017}${this._config.password?'/'+this._config.database:''}`
    }
    this.connection = await MongoClient.connect(url, {
      poolSize: this._config.poolSize || 50
    })
    this.connection = this.connection.db(this._config.database)
    return 
  }
  static init(config) {
    return new Connecttion(config)
  }
}

module.exports = Connecttion
