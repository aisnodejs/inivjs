const Connection = require('./Connection')
const Builder = require('../Builder')
const { getTableName } = require('../../../global/get-name')
const { ObjectID } = require('mongodb');


class MongoAdapter {

    constructor(config) {
        this._config = config
        this._connection = Connection.init(this._config)
    }

    connect() {
        this._connection.connect()
    }
    getObjectId(id) {
        return new ObjectID('stringId');
    }
    get connection() {
        return this._connection.connection
    }
    _getTableName(model = null) {
        if (!model) {
            throw new Error('NO_TABLE_SELECTED')
        }
        if (typeof model == 'object') {
            return model.tableName()
        }
        return model
    }
    /*
      Generic Adapter Methods (these should be in every adapter)
      select, create, queryBuilder, getJoins, makeRelatable
    /*
  
  
      Builds the mysql query, used query builder and root model class
    */
    select({ model, select, where, limit, joins = [] }) {
        let self = this
        if (select) {
            if (Array.isArray(select)) {
                let tempObj = {}
                select.forEach(field => {
                    tempObj[field] = 1
                })
                select = tempObj
            } else {
                select = {
                    select: 1
                }
            }
        } else {
            select = {}
        }
        return new Promise((resolve, reject) => {
            const collection = self.connection.collection(self._getTableName(model))
            collection.find(where)
                .project(select)
                .toArray(function (error, results) {
                    if (error) return reject(error)
                    if (joins.length > 0) results = this.mergeInJoins(results)
                    resolve(self.makeRelatable(limit === 1 ? results[0] : results, model))
                });

        })
    }

    /*
      create a row in the database
    */
    create({ model, data }) {
        let self = this
        return new Promise((resolve, reject) => {
            const collection = self.connection.collection(self._getTableName(model));
            if (!Array.isArray(data)) {
                data = [data]
            }
            collection.insertMany(data, function (error, result) {
                if (error) return reject(error)
                resolve(self.makeRelatable(result.result, model))
            });
        })
    }

    /*
      create a row in the database
    */
    update({ model, where, data }) {
        let self = this
        return new Promise((resolve, reject) => {
            const collection = self.connection.collection(self._getTableName(model));
            collection.update(where, data, function (error, result) {
                if (error) return reject(error)
                resolve(self.makeRelatable(result.result, model))
            });
        })
    }

    /*
      returns a new query builder instance
    */
    queryBuilder(options) {
        return new Builder(options)
    }

    /*
      creates join query from any model realtionships
      used on eager loads
    */
    getJoins(joins) {
        return joins.map(join => ` INNER JOIN \`${join.includeTable}\` ON ${join.localField} = ${join.remoteField}`)
    }


    /*
      Proxy object that returns item from resulting query
      or will check for a relationship on the model
      and return a promise.
  
      ex
      result.id -> returns `id` on the result Object
  
      result.users
        -> returns users if extists on the object.
           otherwise, checks for `users` function on the
           model and returns the related query promise
    */

    makeRelatable(result, model) {
        if (typeof model == 'string') {
            return result
        } else {
            return new Proxy(result, {
                get(target, name) {
                    if (name in target) return target[name]
                    if (getTableName(name) in target) return target[getTableName(name)]

                    let instance = new model(result)
                    if (name in instance) return instance[name]().result()
                }
            })
        }
    }

    /*
      MYSQL SPECIFIC METHODS
    */


    /*
      Joins nested tables for when eager loading a relationship
  
      converts
      {
        users: { name: 'Bob'},
        chats: {...},
      }
      to
      {
        name: 'Bob',
        chats: {...}
      }
    */
    mergeInJoins(results) {
        return results.map(result => {
            let newResult = {}
            Object.keys(result).forEach((item, index) => {
                if (index === 0) newResult = result[item]
                else newResult[item] = result[item]
            })
            return newResult
        })
    }
}

module.exports = MongoAdapter
