const mysql = require('mysql')

class Connecttion {
  constructor(config) {
    this._config = config
    this.connection = null
  }
  connect() {
    let self = this
    this.connection = mysql.createConnection(this._config)

    this.connection.connect(err => {
      if (err) {
        console.error('error when connecting to db:', err)
      }
    })

    this.connection.on('error', err => {
      console.error('db error', err)
      if (err.code === 'PROTOCOL_CONNECTION_LOST') self.connect()
      else throw err
    })
  }
  static init(config){
    return new Connecttion(config)
  }
}

module.exports = Connecttion
