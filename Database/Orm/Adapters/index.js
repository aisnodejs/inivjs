
class Adapters {
    constructor(config){
        this._config = config
        this._adapters = new Map()
        this._registerAdapters()
    }
    _registerAdapters(){
        this._adapters.set('mysql', require(`./Mysql`))
        this._adapters.set('postgres', require(`./Postgres`))
        this._adapters.set('mongo', require(`./Mongo`))
    }
    /**
     * Get addepter by name
     */
    getAdapter(name){
        if(this._adapters.has(name)){
            return this._adapters.get(name)
        }else{
            throw new Error('ADPTER_NOT_REGISTERED')
        }
    }
}

module.exports = Adapters
