const Manager = require('./Database/Manager')

const manager = new Manager({
    // default connection
    connection: 'mongo',
    // all conecctions
    connections: {
        mysql: {
            adapter: 'mysql',
            host: "localhost",
            user: "root",
            password: "",
            database: "chat",
            socketPath: "",
            port: '',
            max: 10,
        },
        postgres: {
            adapter: 'postgres',
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'chat',
            port: 5432,
            max: 10,
            idleTimeoutMillis: 30000,
        },
        mongo: {
            adapter: 'mongo',
            url: 'mongodb://localhost:27017',
            database: 'chat'
        }
    }
})
async function getChats() {
    try {
        console.log(await manager.table('chats').all())
        await manager.table('chats').create([
            { "id": "1", "user_id": "1", "ip": null, "user_agent": null, "email": null, "messages": "blah" },
            { "id": "2", "user_id": "23", "ip": null, "user_agent": null, "email": null, "messages": null },
            { "id": "3", "user_id": null, "ip": null, "user_agent": null, "email": null, "messages": "blah" },
            { "id": "4", "user_id": "23", "ip": null, "user_agent": null, "email": null, "messages": null },
            { "id": "5", "user_id": null, "ip": null, "user_agent": null, "email": null, "messages": "blah" },
            { "id": "6", "user_id": "23", "ip": null, "user_agent": null, "email": null, "messages": null },
            { "id": "7", "user_id": null, "ip": null, "user_agent": null, "email": null, "messages": "blah" },
            { "id": "8", "user_id": "23", "ip": null, "user_agent": null, "email": null, "messages": null },
            { "id": "9", "user_id": null, "ip": null, "user_agent": null, "email": null, "messages": "blah" },
            { "id": "10", "user_id": "23", "ip": null, "user_agent": null, "email": null, "messages": null },
            { "id": "11", "user_id": null, "ip": null, "user_agent": null, "email": null, "messages": "blah" },
            { "id": "12", "user_id": "23", "ip": null, "user_agent": null, "email": null, "messages": null }
        ])
    } catch (e) {
        console.log(e)
    }
}

setTimeout(function () {
    getChats();
}, 1000)