#! /usr/bin/env node
const fs = require('fs')

let commands = {
  'make:migration': require('./migrations/make'),
  'make:model': require('./models/make'),
}

commands[process.argv[2]]({
  args: process.argv.slice(3),
  cwd: process.cwd(),
  fs
})
