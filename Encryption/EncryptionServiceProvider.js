'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ServerServiceProvider Class
 */
class EncryptionServiceProvider {
    constructor(iniv) {
        this._iniv = iniv
    }
    async register() {
        let self = this
        /*
		Register the http server to the system
		*/
        self._iniv._ioc.singleton('Iniv/Encryption', () => {
            const Config = self._iniv._ioc.load('Iniv/Config')
            const Encryption = require('./');
            return new Encryption(Config.get('app.appKey'))
        }).aliase('Encryption')
        return 
    }
    async boot() {
        return true
    }
}
module.exports = EncryptionServiceProvider