'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * GenericError Class
 */
const Youch = require('youch')
class WildCard {
    async handle(error, { request, response }) {
        const youch = new Youch(error, request.request)
        await youch.toHTML().then((html) => {
            response.send(html)
        })
    }

    async report(error, { request, response, auth }) {
        
    }
}
module.exports = WildCard