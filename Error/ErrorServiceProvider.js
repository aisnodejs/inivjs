'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ErrorServiceProvider Class
 */
class ErrorServiceProvider{
    constructor (iniv) {
        this._iniv = iniv;
    }

    register() {
        let self = this
        self._iniv._ioc.bind('Iniv/Error/GenericError', () => {
            return require('./GenericError');
        }).aliase('Helper')
        self._iniv._ioc.singleton('Iniv/Error/Exception', () => {
            let Logger = self._iniv._ioc.load('Iniv/Logger');
            let Helper = self._iniv._ioc.load('Iniv/Helper');
            return new (require('./Exception'))(Logger, Helper, self._iniv._ioc);
        }).aliase('Exception')
        self._iniv._ioc.singleton('Iniv/Error/ErrorHandler', () => {
            return new (require('./ErrorHandler'))();
        }).aliase('ErrorHandler')
    }
    async boot() {
        let self = this
        self._iniv._ioc.load('Iniv/Error/ErrorHandler');
        self._iniv._ioc.load('Iniv/Error/Exception').load();
        self._iniv._ioc.load('Iniv/Error/Exception').bind('*', 'WildCard');
        return true
    }
}

module.exports = ErrorServiceProvider;