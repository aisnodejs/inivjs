'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * GenericError Class
 */
const InvalidArgumentException = require('./InvalidArgumentException')
const RuntimeException = require('./RuntimeException')
const { HttpException, LogicalException } = require('node-exceptions')

module.exports = {
  InvalidArgumentException,
  RuntimeException,
  HttpException,
  LogicalException
}