'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * EventServiceProvider Class
 */
const EventEmitter = require('events');
class Event extends EventEmitter {
    constructor(Helper) {
        super();
        this.Helper = Helper
		this._handlers = new Map()
    }
    fire (eventName, ...arg) {
        this.emit(eventName, ...arg);
    }
	register () {
        var self = this
        let listeners = new (require(self.Helper.eventPath+'/Listeners'))()
        listeners.events = new Map();
        listeners.register();
        listeners.events.forEach(function(handler, key){
            if(typeof  handler == 'function'){
                self.addListener(key, handler);
            }else if(handler instanceof Array){
                handler.forEach(function(listener){
                    listener = require(self.Helper.eventPath+'/Handlers/'+listener)
                    listener = new listener()
                    self.addListener(key, listener.handler);
                })
            }else{
                let listener = require(require(self.Helper.eventPath+'/Handlers/'+handler))
                listener = new listener()
                self.addListener(key, listener.handler);
            }
		})
	}
    
}

module.exports = Event