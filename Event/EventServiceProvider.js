'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * EventServiceProvider Class
 */
class EventServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
        /*
		Regster the service to the system
		*/
		self._iniv._ioc.singleton('Iniv/Event', () => {
            let Helper = self._iniv._ioc.load('Iniv/Helper');
			return new (require('./Event'))(Helper);
        }).aliase('Event')
        return
    }
    async boot () {
        let self = this
        self._iniv._ioc.load('Iniv/Event').register()
        // global['Event'] = self._iniv._ioc.load('Iniv/Event')
        return true
    }
}
module.exports = EventServiceProvider