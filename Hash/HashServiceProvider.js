'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ServerServiceProvider Class
 */
class ServerServiceProvider {
    constructor(iniv) {
        this._iniv = iniv
    }
    async register() {
        let self = this
        /*
		Register the http server to the system
		*/
        self._iniv._ioc.singleton('Iniv/Hash', () => {
            return require('./');
        }).aliase('Hash')
        return 
    }
    async boot() {
        return true
    }
}
module.exports = ServerServiceProvider