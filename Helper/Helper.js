'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Helper Class
 */
class Helper {
    constructor(rootPath, appPath) {
        this.fs = require('fs');
        this.path = require('path');
        this.os = require('os');
        this.url = require('url');
        this._ = require('lodash');
        this.async = require('async');
        this.util = require('util');
        this.requireAll = require('require-all');
        this._rootPath = rootPath;
        this._appPath = appPath;

        this.configPath = this._appPath + '/Configs'
        this.modelPath = this._appPath + '/App/Models'
        this.routePath = this._appPath + '/App/Http/Routes'
        this.httpKernelPath = this._appPath + '/App/Http/kernel'
        this.controllerPath = this._appPath + '/App/Http/Controllers'
        this.validatiorPath = this._appPath + '/App/Http/Validators'
        this.exceptionsHandlersPath = this._appPath + '/App/Exceptions/Handlers'
        this.eventPath = this._appPath + '/App/Events'
        this.publicPath = this._appPath + '/public'

        this.socketChannels = this._appPath + '/App/Ws/Channels'
        this.socketKernel = this._appPath + '/App/Ws/kernel'

    }
    promisify(callback) {
        return this.util.promisify(callback)
    }
    tmpPath() {
        return this.os.tmpdir()
    }
    /**
     * makes complete namespace for a given path and base
     * namespace
     *
     * @method makeNameSpace
     *
     * @param  {String}      baseNameSpace
     * @param  {String}      toPath
     * @return {String}
     *
     * @public
     */
    appNameSpace() {
        return 'App'
    }
    makeNameSpace(baseNameSpace, toPath) {
        const appNameSpace = this.appNameSpace()
        if (toPath.startsWith(`${appNameSpace}/`)) {
            return toPath
        }
        return this.path.normalize(`${appNameSpace}/${baseNameSpace}/${toPath}`)
    }
    appPath(path = '') {
        return this.path.join(this._appPath, path)
    }
    rootPath(path = '') {
        return this.path.join(this._rootPath, path)
    }
    resourcePath(path = '') {
        return this.path.join(this._appPath, './Resources', path)
    }
    viewPath(path = '') {
        return this.path.normalize(this.path.join(this._appPath, '/App/Views/', path))
    }
    developerRoute(uri = '') {
        if (uri == '') {
            return "developers"
        } else {
            return "developers/" + uri
        }
    }
    /**
     * Get all function of Object
     */
    getAllFuncs(obj) {
        let props = []

        do {
            const l = Object.getOwnPropertyNames(obj)
                .concat(Object.getOwnPropertySymbols(obj).map(s => s.toString()))
                .sort()
                .filter((p, i, arr) =>
                    typeof obj[p] === 'function' &&  //only the methods
                    p !== 'constructor' &&           //not the constructor
                    (i == 0 || p !== arr[i - 1]) &&  //not overriding in this prototype
                    props.indexOf(p) === -1          //not overridden in a child
                )
            props = props.concat(l)
        }
        while (
            (obj = Object.getPrototypeOf(obj)) &&   //walk-up the prototype chain
            Object.getPrototypeOf(obj)              //not the the Object prototype methods (hasOwnProperty, etc...)
        )

        return props
    }

}

module.exports = Helper