'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Http Server Class
 */
class HelperServiceProvider{
    constructor (iniv) {
        this._iniv = iniv;
    }

    async register() {
        let self = this
        self._iniv._ioc.singleton('Iniv/Helper', () => {
            return new (require('./Helper'))(self._iniv._ioc._rootPath,self._iniv._ioc._appPath);
        }).aliase('Helper')
        return
    }
    async boot() {
        return true
    }
}

module.exports = HelperServiceProvider;