'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Service Class
 */
class Service {
	constructor(name, factory, singleton = false, type) {
		this.name = name;
		this.factory = factory;
		this.isSingleton = singleton;
		this.type = type;
		this.aliaseName = null;
		this._instance = null;
	}
	aliase(value) {
		this.aliaseName = value
	}
}
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Ioc Class
 */
class Ioc {
	// Constructor of the application
	constructor(rootPath, appPath) {
		this._rootPath = rootPath
		this._appPath = appPath
		this._services = new Map()
		this._nameSpaces = {}

		global['load'] = (serviceName, ...arg) => {
			return this.load(serviceName, ...arg)
		}
		this.singleton('Iniv/Ioc', () => { }, this).aliase('Models')
	}
	// singleton new factory
	singleton(serviceName, factory, instance = false) {
		this._services.set(serviceName, new Service(serviceName, factory, true, 'factory'))
		if (instance) {
			this._services.get(serviceName)['_instance'] = instance
		}
		return this._services.get(serviceName)
	}
	// register new factory
	register(serviceName, factory) {
		this._services.set(serviceName, new Service(serviceName, factory, false, 'factory'))
		return this._services.get(serviceName)
	}
	bind(serviceName, factory) {
		this._services.set(serviceName, new Service(serviceName, factory, false, 'factory'))
		return this._services.get(serviceName)
	}

	// resolve the service
	load(serviceName, ...arg) {
		let self = this
		var service = this._services.get(serviceName)
		if (!service) {
			try {
				return require(self._appPath + '/' + serviceName)
			} catch (e) {
				throw new Error('No service register with this name ' + serviceName)
			}
		}
		if (service._instance) {
			return service._instance
		}
		var instance = service.factory(...arg)
		if (service.isSingleton) {
			service._instance = instance
		}
		return instance
	}
	resolveFunc(nameSpace) {
		let self = this
		if (typeof nameSpace == 'function') {
			return { method: nameSpace };
		}
		try {
			if (this.load(nameSpace.split('.')[0])) {
				let handler = this.load(nameSpace.split('.')[0])
				return { method: handler[nameSpace.split('.')[1]].bind(handler) }
			}
		} catch (e) {
			let newClass = require(this.load('Iniv/Helper').path.join(this.load('Iniv/Helper').appPath(), nameSpace.split('.')[0]))
			let params = []
			let inject = newClass.inject()
			if (inject) {
				inject.forEach(function (service) {
					params.push(self.load(service))
				})
			}
			let handler = new newClass(...params);
			return { method: handler[nameSpace.split('.')[1]].bind(handler) }
		}

	}
	resolveControllerFunc(nameSpace) {
		if (typeof nameSpace == 'function') {
			return { method: nameSpace };
		}
		try {
			let method = this.load(nameSpace.split('.')[0])
			method = new method();
			return { method: method[nameSpace.split('.')[1]].bind(method) }
		} catch (e) {
			try {
				let method = require(this.load('Iniv/Helper').path.join(this.load('Iniv/Helper').controllerPath, nameSpace.split('.')[0]))
				method = new method();
				return { method: method[nameSpace.split('.')[1]].bind(method) }
			} catch (e) {
				return e
			}
		}
	}
	resolveValidator(validator) {
		try {
			return new (require(this.load('Iniv/Helper').path.join(this.load('Iniv/Helper').validatiorPath, validator)))()
		} catch (e) {
			return e
		}
	}
	resolveSocketController(closure) {
		try {
			if (this.load(closure)) {
				return this.load(closure)
			}
		} catch (e) {
			try {
				return require(this.load('Iniv/Helper').path.join(this.load('Iniv/Helper').appPath(closure)))
			} catch (e) {
				return e
			}
		}
		
	}
	resolveBinding(nameSpace, handler) {
		try {
			return new (require(this.load('Iniv/Helper').path.join(nameSpace, handler)))()
		} catch (e) {
			try {
				return this.load('Iniv/Error/' + handler)
			} catch (e) {
				return this.load('Iniv/Error/ErrorHandler')
			}
		}
	}
	make(service) {
		if (service.inject) {
			let args = []
			service.inject.forEach(element => {
				args.push(load(element))
			});
			return new service(...args)
		} else {
			return new service()
		}
	}
}
module.exports = Ioc