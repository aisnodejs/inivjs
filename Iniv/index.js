'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Iniv Class
 */

const Ioc = require('./Ioc');

class Inivjs {
    constructor(appPath, rootPath) {
        this._ioc = new Ioc(rootPath, appPath);
        this._providers = new Map();
        this.register()
            .catch((error) => {
                console.error(error)
            });
    }

    async register() {
        let self = this
        let providers = [
            'Helper/HelperServiceProvider',
            'Config/ConfigServiceProvider',
            'Timer/TimerServiceProvider',
            'Logger/LoggerServiceProvider',
            'Localize/LocalizeServiceProvider',
            'Context/ContextServiceProvider',
            'Hash/HashServiceProvider',
            'Encryption/EncryptionServiceProvider',
            'Event/EventServiceProvider',
            'Error/ErrorServiceProvider',
            'Request/RequestServiceProvider',
            'Response/ResponseServiceProvider',
            'Server/ServerServiceProvider',
            'WebSocket/WsServiceProvider',
            'View/ViewServiceProvider',
            'Mail/MailServiceProvider',
            'Auth/AuthServiceProvider',
            'BodyParser/BodyParserServiceProvider',
            'Session/SessionServiceProvider',
            'Validation/ValidationServiceProvider',
            'Model/ModelServiceProvider',
            'Database/DatabaseServiceProvider',
            'Route/RouteServiceProvider',
            'View/ViewServiceProvider',
            'Menu/MenuServiceProvider',
        ]
        for (const provider of providers) {
            self._providers.set(provider, new (require('../' + provider))(self))
            await self._providers.get(provider).register()
        }
        if (this._ioc.load('Iniv/Config').get('app.plugins')) {
            for (let index = 0; index < this._ioc.load('Iniv/Config').get('app.plugins').length; index++) {
                const provider = this._ioc.load('Iniv/Config').get('app.plugins')[index];
                let Service = null
                try {
                    Service = require(provider + '/ServiceProvider');
                } catch (error) {
                    console.log(error.message)
                }
                if (Service) {
                    Service = new Service()
                    Service.Ioc = self._ioc
                    self._providers.set(provider, Service)
                    await self._providers.get(provider).register()
                }
            }
        }
        if (this._ioc.load('Iniv/Config').get('app.providers')) {
            for (let index = 0; index < this._ioc.load('Iniv/Config').get('app.providers').length; index++) {
                const provider = this._ioc.load('Iniv/Config').get('app.providers')[index];
                let Service = null
                try {
                    Service = require(self._ioc.load('Iniv/Helper').appPath(provider));
                } catch (error) {
                    console.log(error.message)
                }
                if (Service) {
                    Service = new Service()
                    Service.Ioc = self._ioc
                    self._providers.set(provider, Service)
                    await self._providers.get(provider).register()
                }
            }
        }
        await this.boot()
    }

    async boot() {
        let self = this
        for (const [key,provider] of self._providers) {
            let val = await provider.boot()
        }
        load('Iniv/Server').listen(load('Iniv/Config').get('http.host'), load('Iniv/Config').get('http.port'))
    }
}

module.exports = Inivjs