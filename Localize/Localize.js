'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Localize Class
 */

const Translator = require('./Translator');

class Localize {
	constructor(Helper, Config) {
		this.Helper = Helper;
		this.Config = Config;
		this._locals = {}
		this.Config.get('app.locals').forEach(local=>{
			this._locals[local] = {}
		})
		this._local = this.Config.get('app.defaultLocal')
	}
	addLocal(name) {
		if (this._locals[name]) {
			return this._locals[name]
		}
		return this._locals[name] = {}
	}
	getLocal(name) {
		return this._local
	}
	getLocals() {
		return Object.keys(this._locals)
	}
	getTranslations(){
		return this._locals
	}
	loadLocals(path) {
		let allLocals = this.Helper.requireAll(path)
		for (const local in allLocals) {
			if (allLocals.hasOwnProperty(local)) {
				const element = allLocals[local];
				Object.assign(this.addLocal(local), element)
			}
		}
		return this
	}
	setLocal(name) {
		if (!this._locals[name]) {
			throw new Error(`${name} Local not registered in system`)
		}
		this._local = name
		return this
	}
	translate(key, lang) {
		let value = this.Helper._.get(this._locals[lang?lang:this._local], key, key)
		return value
	}
}

module.exports = Localize