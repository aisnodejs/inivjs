'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * LocalizeServiceProvider Class
 */

class LocalizeServiceProvider {
    constructor(iniv) {
        this._iniv = iniv
    }
    async register() {
        let self = this
        /*
		Regster the logger to the system
		*/
        self._iniv._ioc.singleton('Iniv/Localize', () => {
            let Helper = self._iniv._ioc.load('Iniv/Helper');
            let Config = self._iniv._ioc.load('Iniv/Config');
            return new (require('./Localize'))(Helper, Config);
        }).aliase('Localize')
        return
    }
    async boot() {
        let self = this
        const Localize = self._iniv._ioc.load('Iniv/Localize')
        const HttpContext = this._iniv._ioc.load('Iniv/HttpContext')
        const WsContext = this._iniv._ioc.load('Iniv/WsContext')

        Localize.loadLocals(self._iniv._ioc.load('Iniv/Helper').resourcePath('locals'))
        /**
         * Registers an isolated instance of loader on the
         * response object. Each view has access to
         * the request object.
         */
        HttpContext.getter('localize', function () {
            return Localize
        }, true)

        WsContext.getter('localize', function () {
            return Localize
        }, true)
        HttpContext.getter('translate', function () {
            return Localize.translate.bind(Localize)
        }, true)

        WsContext.getter('translate', function () {
            return Localize.translate.bind(Localize)
        }, true)
        return true
    }
}
module.exports = LocalizeServiceProvider