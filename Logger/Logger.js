'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Logger Class
 */

const winston = require('winston');
winston.addColors({
	silly: 'magenta',
	debug: 'blue',
	verbose: 'cyan',
	info: 'green',
	data: 'grey',
	warn: 'yellow',
	error: 'red'
});
class Logger {
	constructor(Helper, Config) {
		this.Helper = Helper;
		this.Config = Config;

		this.logger = new (winston.Logger)({
			exitOnError: false,
			levels: { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 },
		})
	}
	silly() {
		return this.logger['silly'].apply(this.logger, arguments);
	}
	verbose() {
		return this.logger['verbose'].apply(this.logger, arguments);
	}
	debug() {
		return this.logger['debug'].apply(this.logger, arguments);
	}
	info() {
		return this.logger['info'].apply(this.logger, arguments);
	}
	warn() {
		return this.logger['warn'].apply(this.logger, arguments);
	}
	error() {
		return this.logger['error'].apply(this.logger, arguments);
	}

	boot() {
		let self = this
		// this.logger.winston.remove(winston.transports.Console)
		if (self.Config.get('logger.console', 'true')) {
			this.logger.add(winston.transports.Console, {
				level: self.Config.get('logger.level', 'silly'),
				prettyPrint: true,
				colorize: true,
				silent: false,
				timestamp: self.Config.get('logger.timestamp', false)
			});
		}
		if (self.Config.get('logger.file', 'true')) {
			this.logger.add(winston.transports.File, {
				level: self.Config.get('logger.level', 'silly'),
				silent: false,
				prettyPrint: true,
				maxsize: self.Config.get('logger.maxsize', 15000000),
				filename: self.Config.get('logger.filePath', self.Helper.appPath('logfile.log')),
				timestamp: self.Config.get('logger.timestamp', true)
			});
		}
		this.transport = this.logger.transports[this.Helper._.keys(this.logger.transports)[1]];
	}
	list(options) {
		var self = this;
		return new Promise(function (resolve, reject) {
			self.transport.query(options, function (error, logs) {
				if (error) {
					return reject(error);
				}
				resolve(logs);
			});

		});
	}
	
	count() {
		var self = this;
		return new Promise(function (resolve, reject) {
			self.transport.query({rows:15000000000}, function (error, logs) {
				if (error) {
					return reject(error);
				}
				resolve(logs.length);
			});

		});
	}
}

module.exports = Logger