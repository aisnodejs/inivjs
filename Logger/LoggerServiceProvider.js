'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * LoggerServiceProvider Class
 */

 class LoggerServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
        /*
		Regster the logger to the system
		*/
		self._iniv._ioc.singleton('Iniv/Logger', () => {
            let Helper = self._iniv._ioc.load('Iniv/Helper');
            let Config = self._iniv._ioc.load('Iniv/Config');
			return new (require('./Logger'))(Helper,Config);
        }).aliase('Logger')
        return
    }
    async boot () {
        let self = this 
        self._iniv._ioc.load('Iniv/Logger').boot()
        global['Logger'] = self._iniv._ioc.load('Iniv/Logger')
        return true
    }
}
module.exports = LoggerServiceProvider