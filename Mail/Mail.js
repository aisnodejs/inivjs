var nodemailer = require('nodemailer');

class Mail {
    constructor(Config) {
        this.transporter = nodemailer.createTransport(Config.get('mail.smtp'), Config.get('mail.options'))
    }
    verify(callback) {
        this.transporter.verify(callback);
    }
    async send(message) {
        return await this.transporter.sendMail(message)
    }
}

module.exports = Mail