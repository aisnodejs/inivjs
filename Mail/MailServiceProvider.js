class MailServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
        /*
		Regster the logger to the system
		*/
		self._iniv._ioc.singleton('Iniv/Mail', () => {
            let Config = self._iniv._ioc.load('Iniv/Config');
			return new (require('./Mail'))(Config);
        }).aliase('Mail')
        return
    }
    async boot () {
        let self = this
        return true
    }
}
module.exports = MailServiceProvider