'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */
/**
 * Menu Node Class Requirement
 *
 * @type {MenuNode}
 */
var MenuNode = require('./MenuNode');

/**
 * Menu Class
 */
class Menu {
    constructor(name, activClass = 'active', parentClass = "", textClass = "") {
        // Menu Name
        this.name = name;
        /**
         * List of Child Nodes
         * @type {MenuNode[]}
         */
        this.nodeList = [];
        this.activClass = activClass
        this.parentClass = parentClass
        this.textClass = textClass
        /**
         * HTML Attributes Array
         * @type {Array}
         */
        this.htmlAttributes = [];

        /**
         * Depth
         * @type {int}
         */
        this.depth = -1;

        /**
         * HTML Sourcery Generator
         */
        this.generator = require('html-sourcery');
        /**
         * Current Request
         * @type {request}
         */
        this.currentRequest = null;

        /**
         * Current User
         * @type {user}
         */
        this.currentUser = null;
    }
    /**
     * Add a New Menu Node
     *
     * @param text
     * @param route
     * @returns {MenuNode}
     */
    addMenuNode(text, route, role, elementType = 'li') {
        // New Node
        var newNode = new MenuNode(this, this);
        // Assign Variables
        newNode.text = text;
        newNode.route = route;
        newNode.elementType = elementType;
        // Add to List
        newNode.role = role
        this.nodeList.push(newNode);
        // Return
        return newNode;
    };

    /**
     * Get Current Request Route Path
     * @returns {String}
     */
    getCurrentRequestRoute() {
        return this.currentRequest.url();
    }
    /**
     * Get Current Request User
     * @returns {String}
     */
    getCurrentUserHasRole(roles) {
        if (typeof roles == 'undefined') {
            return true
        }
        if (!this.currentUser){
            return false
        }
        if (typeof roles == 'string') {
            return this.currentUser.role == roles
        }
        if (typeof roles == 'object' && Array.isArray(roles) && roles.indexOf(this.currentUser.role) != -1) {
            return true
        }
        return false
    }
    /**
     * Render Menu to String
     * Can be called separately or with something like Jade by calling the menuname
     *
     * @returns {String}
     */
    toString(request, user) {
        this.currentRequest = request
        this.currentUser = user
        // Init Child Html
        var childHtml = [];
        // Node List for Reference Below
        var nodeList = this.nodeList;
        // Generate Children HTML Recursively
        this.nodeList.forEach(function (childNode, key) {
            // Handle First and Last
            if (key == 0) {
                childNode.isFirst = true;
            }
            if (key == (nodeList.length - 1)) {
                childNode.isLast = true;
            }
            // Append
            if (childNode.toHtml()) {
                childHtml.push(childNode.toHtml());
            }
        });

        // Wrap in Menu HTML, Compile and Return
        return this.generator.ul(
            this.htmlAttributes,
            childHtml
        ).compile();
    }
    /**
     * Set HTML Attributes
     *
     * @param attributes
     * @returns {exports}
     */
    setAttributes(attributes) {
        this.htmlAttributes = attributes;
        return this;
    }

}
/**
* MenuManager Class
*/
class MenuManager {

    constructor(Helper, Config, Ioc) {
        this.Config = Config
        this.Helper = Helper
        this.Ioc = Ioc
        this.menuList = {}

    }
    /**
     * create new menu
     *
     * @param name
     */
    addMenu(name, activClass, parentClass, textClass) {
        if (this.menuList[name]) {
            return this.menuList[name]
        }
        this.menuList[name] = new Menu(name, activClass, parentClass, textClass)
        return this.menuList[name]
    }
    /**
     * Middleware Function to add menu items in view
     * @param request
     */
    getMenus(request, user) {
        let menus = {}
        Object.keys(this.menuList).forEach(name => {
            menus[name] = this.menuList[name].toString(request, user)
        })
        return menus
    }

    async createMenu(name, option, menuJson) {
        let self = this
        if (typeof menuJson != 'object') {
            throw new Error('INVALID_ARGUMENT')
            return
        }
        if (!Array.isArray(menuJson)) {
            throw new Error('INVALID_ARGUMENT')
            return
        }
        let menu = self.addMenu(name, option.activClass, option.parentClass, option.textClass);
        menu.setAttributes({
            class: option.class,
            id: option.id
        });

        for (let i = 0; i < menuJson.length; i++) {
            const menuItem = menuJson[i];
            let menuNode = menu.addMenuNode(menuItem.text, menuItem.url, menuItem.role);
            menuNode.setAttributes({ class: menuItem.class, id: menuItem.id });
            if (menuItem.children) {
                let children = []
                if (typeof menuItem.children == 'object' && Array.isArray(menuItem.children))
                    children = menuItem.children

                if (typeof menuItem.children == 'function') {
                    children = await menuItem.children()
                }
                children.forEach(item => {
                    menuNode.addMenuNode(item.text, item.url, item.role ? item.role : menuItem.role)
                        .setAttributes({ class: item.class, id: item.id }).setIcon(item.iconClass);
                })
            }
            menuNode.setIcon(menuItem.iconClass)
        }
    }
}

module.exports = MenuManager
