/**
 * Active Menu Node Class
 *
 * @param menuInstance
 * @param parentNode
 * @constructor
 */
var MenuNode = module.exports = function (menuInstance, parentNode, elementType="ul") {

    /**
     * Parent Node
     * @type {ActiveMenu | MenuNode}
     */
    this.parent = parentNode;

    /**
     * Menu Instance
     * @type {ActiveMenu}
     */
    this.menuInstance = menuInstance;

    /**
     * Display Text
     * @type {String}
     */
    this.text = null;

    /**
     * Route
     * @type {String}
     */
    this.route = null;

    /**
     * Element Type
     * @type {string}
     */
    this.elementType = 'ul';

    /**
     * Children Nodes
     * @type {MenuNode[]}
     */
    this.childNodes = [];

    /**
     * HTML Attributes
     * @type {Array}
     */
    this.htmlAttributes = [];

    /**
     * Menu Level / Depth
     * @type {number}
     */
    this.depth = parentNode.depth + 1;

    /**
     * Icon Class
     * @type {string}
     */
    this.iconclass = 'fa fa-home'
    if(this.depth > 0){
        this.iconclass = 'fa fa-circle-o'
    }
    /**
     * Whether This Node is Active
     * @type {boolean}
     */
    this.isActive = false;
    /**
     * classes
     */
    this.activClass = this.menuInstance.activClass;
    
    this.parentClass = this.menuInstance.parentClass;
    
    this.textClass = this.menuInstance.textClass;
    /**
     * Whether This Node is First In The Current Level
     * @type {boolean}
     */
    this.isFirst = false;
    
    /**
     * Whether This Node is Last In The Current Level
     * @type {boolean}
     */
    this.isLast = false;
};

/**
 * Set HTML Attributes
 *
 * @param attributes
 * @returns {MenuNode}
 */
MenuNode.prototype.setAttributes = function(attributes) {
    this.htmlAttributes = attributes;
    return this;
};
/**
 * Set Icon for node
 *
 * @param attributes
 * @returns {MenuNode}
 */
MenuNode.prototype.setIcon = function(iconclass) {
    this.iconclass = iconclass;
    return this;
};

/**
 * Check If This Node Is a List
 *
 * @returns {boolean}
 */
MenuNode.prototype.isList = function() {
    return this.elementType == 'ul';
};

/**
 * Check If This Node is a Parent
 */
MenuNode.prototype.isParent = function() {
    return this.childNodes.length > 0;
};

/**
 * Add a Child Node to This Node
 *
 * @param text
 * @param route
 * @returns {MenuNode}
 */
MenuNode.prototype.addMenuNode = function (text, route,role, elementType='li') {
    var newNode = new MenuNode(this.menuInstance, this);
    newNode.text = text;
    newNode.route = route;
    newNode.role = role
    if (this.isList()) {
        newNode.elementType = elementType;
    }
    this.childNodes.push(newNode);
    return newNode;
};

/**
 * Activate This Branch of the Menu Tree.
 *
 * Iterates up the parents of this node, activating them
 * by appending a class of 'active' to their list element.
 */
MenuNode.prototype.activateBranch = function() {
    this.isActive = true;
    // Stop At The Top of The Branch
    if (this.parent.hasOwnProperty('activateBranch')) {
        this.parent.activateBranch();
    } else {
        this.parent.isActive = true;
    }
};

/**
 * Get The Render-able HTML Attributes
 *
 * @returns {Array}
 */
MenuNode.prototype.getRenderHtmlAttributes = function() {

    // Attributes
    var htmlAttributes = [];
    for (var key in this.htmlAttributes) {
        htmlAttributes[key] = this.htmlAttributes[key];
    }

    // Get Class Array
    var htmlClassArray = [];

    // If Classes Are Already Defined, We Need to Append Rather Than Replace
    if (this.htmlAttributes.hasOwnProperty('class')) {
        htmlClassArray = this.htmlAttributes.class.split(' ');
    }

    // Handle Active State
    if (this.isActive) {
        htmlClassArray.push(this.activClass);
    }

    // Handle Case of Being Only Child, Then First, Then Last
    if (this.isFirst && this.isLast) {
        htmlClassArray.push('only');
    } else if (this.isFirst) {
        htmlClassArray.push('first');
    } else if (this.isLast) {
        htmlClassArray.push('last');
    }

    // Handle Case of Being A Parent
    if (this.isParent()) {
        htmlClassArray.push(this.parentClass);
    }

    // Join Class List
    htmlAttributes.class = htmlClassArray.join(' ');

    // Return
    return htmlAttributes;
};

/**
 * Get HTML Attributes for a List Element (<ul>)
 * @returns {Array}
 */
MenuNode.prototype.getListHtmlAttributes = function()
{
    // Html Attributes
    var htmlAttributes = [];
    // HTML Classes
    var htmlClasses = [];
    // Handle Active State
    if (this.isActive) {
        htmlClasses.push(this.activClass);
    }
    // Handle Depth
    if(this.depth > 0){
        htmlClasses.push(this.activClass);
    }
    // Add Classe
    htmlAttributes.class = htmlClasses.join(' ');
    // Return
    return htmlAttributes;
};

/**
 * Generate The Inner HTML For This Node
 * @returns {String}
 */
MenuNode.prototype.getInnerHtml = function() {

    // Inner Element Type
    var elementType;
    if (this.route) {
        elementType = "a";
    } else {
        elementType = "span";
    }

    // Html Attributes
    var htmlAttributes = [];
    htmlAttributes.href = this.route;
    // Check Current Route
    if (this.route == this.menuInstance.getCurrentRequestRoute() || this.menuInstance.getCurrentRequestRoute().startsWith(this.route)) {
        // Add Active Link Class to Link
        htmlAttributes.class = this.activClass;
        // Activate Parents
        this.activateBranch();
    }
    // Inner Html
    if (this.textClass){
        this.text = this.menuInstance.generator.span(
            { class: this.textClass }, [
                this.text
            ],
        )
    }
    var innerHtml = this.menuInstance.generator[elementType](
        htmlAttributes,
        [
        this.menuInstance.generator.tag('i',
        { class: this.iconclass },[]
        ),
        this.text
    ]
    );

    // If A List, Inner HTML Must Be Wrapped in a List Item
    if (this.isList()) {
        innerHtml = this.menuInstance.generator.li(
            this.getRenderHtmlAttributes(),
            innerHtml
        );
    }

    // Compile and Return
    return innerHtml.compile();
};

/**
 * Generate the HTML for this Node
 * @returns {String}
 */
MenuNode.prototype.toHtml = function() {
    let self = this
    // New HTML Tree
    var elementInnerHtml = [];

    if (!self.menuInstance.getCurrentUserHasRole(self.role)) {
        return null
    }
    // Set To Inactive (Prevents Caching of Active State on Nodes That Aren't Cached)
    this.isActive = false;
    // Inner Html
    elementInnerHtml.push(this.getInnerHtml());
    
    // Node List for Reference Below
    var nodeList = this.childNodes;
    // Render Children Source
    this.childNodes.forEach(function(childNode, key) {
        if (!self.menuInstance.getCurrentUserHasRole(childNode.role)){
            return
        }
        // Handle First and Last
        if (key == 0) {
            childNode.isFirst = true;
        }
        if (key == (nodeList.length - 1)) {
            childNode.isLast = true;
        }
        // Append
        elementInnerHtml.push(childNode.toHtml());
    });
    // Handle List Attributes
    var htmlAttributes;
    if (this.isList()) {
        htmlAttributes = this.getListHtmlAttributes();
    } else {
        htmlAttributes = this.getRenderHtmlAttributes();
    }
    
    // Render
    var elementHtml = this.menuInstance.generator[this.elementType](
        htmlAttributes,
        elementInnerHtml
    );
    // Compile and Return
    return elementHtml.compile();
};