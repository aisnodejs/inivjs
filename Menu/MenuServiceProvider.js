'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Request Class
 */
class MenuServiceProvider {
    constructor(iniv) {
        this._iniv = iniv
    }
    async register() {
        let self = this
        /*
		Register the http model to the system
		*/
        self._iniv._ioc.singleton('Iniv/MenuManager', () => {
            const Config = self._iniv._ioc.load('Iniv/Config')
            const Helper = self._iniv._ioc.load('Iniv/Helper')
            return new (require('./MenuManager'))(Helper, Config, self._iniv._ioc);
        }).aliase('MenuManager')
        self._iniv._ioc.bind('Iniv/Middleware/Menu', () => {
            const Menu = require('./Middleware')
            return new Menu(load('Iniv/Config'))
        })
        return 
    }
    async boot() {
        return true
    }
}
module.exports = MenuServiceProvider