'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */


class Menu {
  constructor (Config) {
    
  }

  /**
   * Generate menu for the application
   *
   * @method handle
   *
   * @param  {Object}   options.request
   * @param  {Function} next
   *
   * @return {void}
   */
  async handle ({ request, view, auth }, next) {
    Logger.silly('creating menu try for all menu')
    let menus = load('Iniv/MenuManager').getMenus(request, auth.user)
    if (view && typeof (view.share) === 'function') {
      view.share({
        menus: menus
      })
    }

    await next()
  }

}

module.exports = Menu
