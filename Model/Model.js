const fs = require('fs');
const path = require('path')
const Waterline = require('waterline');

class Models {
	constructor (Helper, Config, Ioc) {
		this.Helper = Helper
		this.Config = Config
		this.Ioc = Ioc
        this._models = new Map()
        this.Waterline = new Waterline()
	}
	registerModel (modelClassPath, nameSpace = ''){
		var self = this
		let modelClass = require(modelClassPath)
		let modelObj = new modelClass()
		modelObj.identity = modelClass.identity()
		modelObj.primaryKey = modelClass.primaryKey()
		modelObj.schema = modelClass.schema()
		modelObj.attributes = self.Helper.util._extend({},modelClass.defaultAttributes())
		Object.assign(modelObj.attributes, modelClass.attributes())
		self.Helper.getAllFuncs(modelObj).forEach((method)=>{
			modelObj[method] = modelObj[method]
		})
		self.Waterline.registerModel(Waterline.Collection.extend(modelObj))
		nameSpace = nameSpace==''?this.Helper.path.parse(modelClassPath).dir:nameSpace
		self._models.set(this.Helper.path.basename(modelClassPath).split('.')[0], { nameSpace : nameSpace, identity : modelObj.identity,model:modelObj})
	}
    load () {
		var self = this
		fs
		.readdirSync(self.Helper.modelPath)
		.filter(function(file) {
			return (file.indexOf(".") !== 0);
		})
		.forEach(function(file) {
			let modelClass = require(path.join(self.Helper.modelPath, file))
			let modelObj = new modelClass()
			modelObj.identity = modelClass.identity()
			modelObj.primaryKey = modelClass.primaryKey()
			modelObj.schema = modelClass.schema()
			modelObj.attributes = self.Helper.util._extend({},modelClass.defaultAttributes())
			Object.assign(modelObj.attributes, modelClass.attributes())
			self.Helper.getAllFuncs(modelObj).forEach((method)=>{
				modelObj[method] = modelObj[method]
			})
            self.Waterline.registerModel(Waterline.Collection.extend(modelObj))
			self._models.set(file.split('.')[0], { nameSpace : self.Helper.modelPath, identity : modelObj.identity,model:modelObj})
		});
		
	}
	
	boot () {
		var self = this
		let prom =  new Promise(function (resolve, reject) {
			self.Waterline.initialize(self.Config.get('database'), function (err, models) {
				if (err) return reject(err);
				self._models.forEach(function (value, key) {
					if (value.nameSpace.includes(self.Helper.appPath())) {
						value.nameSpace = value.nameSpace.replace(self.Helper.appPath() + '/', '') + '/' + key
					} else {
						value.nameSpace = value.nameSpace + '/' + key
					}
					let model = models.collections[self._models.get(key).identity]
					model.getDatastore = function () {
						return this._adapter.datastores[self.Config.get('database.datastore')]
					}.bind(model)
					self.Ioc.singleton(value.nameSpace, () => { }, model)
				})
				return resolve(true)
			});
		})
		return prom
	}
}

module.exports = Models