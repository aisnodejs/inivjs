class ModelServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
        /*
		Register the http model to the system
		*/
		self._iniv._ioc.singleton('Iniv/Database/ModelManager', () => {
            const Config = self._iniv._ioc.load('Iniv/Config')
            const Helper = self._iniv._ioc.load('Iniv/Helper')
			return new (require('./Model'))(Helper, Config, self._iniv._ioc);
        }).aliase('Models')

        self._iniv._ioc.register('Iniv/Database/Model', () => {
            const Config = self._iniv._ioc.load('Iniv/Config')
			return class Model {
                constructor () {
                    this.identity = '';
                    this.datastore = Config.get('database.datastore');
                    this.migrate = Config.get('database.migrate');
                    this.primaryKey = 'id',
                    this.schema = Config.get('models.schema')
                }
                static defaultAttributes () {
                    return Config.get('models.attributes')
                }
            };
        }).aliase('Model')
        self._iniv._ioc.load('Iniv/Database/ModelManager').load()
        return
    }
    async boot () {
        let self = this
        await self._iniv._ioc.load('Iniv/Database/ModelManager').boot()
        return true
    }
}
module.exports = ModelServiceProvider