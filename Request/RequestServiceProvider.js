'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * RequestServiceProvider Class
 */
class RequestServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
        /*
        Register the http server to the system
        */
        self._iniv._ioc.singleton('Iniv/Request', () => {
            return require('./Request');
        }).aliase('Request')
        return
    }
    async boot () {
        return true
    }
}
module.exports = RequestServiceProvider