'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ResponseServiceProvider Class
 */
class ResponseServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
         /*
		Register the http server to the system
		*/
		self._iniv._ioc.singleton('Iniv/Response', () => {
			return require('./Response');
        }).aliase('Response')
        return
    }
    async boot () {
        // boot the service or load the service
        return true
    }
}
module.exports = ResponseServiceProvider