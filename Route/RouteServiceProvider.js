'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * RouteServiceProvider Class
 */
class RouteServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
        /*
        Register the http server to the system
        */
        self._iniv._ioc.singleton('Iniv/Router', () => {
            return new (require('./Router'))();
        }).aliase('Router')
        return
    }
    async boot () {
        let self = this
        self._iniv._ioc.load('Iniv/Helper').requireAll(self._iniv._ioc.load('Iniv/Helper').routePath)
        require(self._iniv._ioc.load('Iniv/Helper').httpKernelPath)
        return true
    }
}
module.exports = RouteServiceProvider