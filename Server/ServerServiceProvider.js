'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ServerServiceProvider Class
 */
class ServerServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
        /*
		Register the http server to the system
		*/
		self._iniv._ioc.singleton('Iniv/Middleware/Static', () => {
            let Helper = self._iniv._ioc.load('Iniv/Helper');
            let Config = self._iniv._ioc.load('Iniv/Config');
			return require('./Static')(Helper,Config);
        }).aliase('Static')
		self._iniv._ioc.singleton('Iniv/Server', () => {
            let Context = self._iniv._ioc.load('Iniv/HttpContext');
            let Router = self._iniv._ioc.load('Iniv/Router');
            let Logger = self._iniv._ioc.load('Iniv/Logger');
            let Exception = self._iniv._ioc.load('Iniv/Error/Exception');
			return new (require('./Server'))(Context,Router,Logger.logger,self._iniv._ioc, Exception);
        }).aliase('Http')
        return
    }
    async boot () {
        let self = this
        return true
    }
}
module.exports = ServerServiceProvider