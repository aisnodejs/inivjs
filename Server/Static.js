'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Static Server Class
 */

const serveStatic = require('serve-static')
const defaultConfig = {
  'index': ['index.html', 'index.htm'],
  'setHeaders': function (res, path) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, HEAD, OPTIONS");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  }
}
let staticServers = []

/**
 * Server middleware to serve static resources. All GET and HEAD
 * requests are handled by this middleware and response is
 * made when there is a resource inside the `public`
 * directory.
 *
 * @class Static
 */
class Static {
  /**
   * The handle method called by Server on each request
   *
   * @method handle
   *
   * @param  {Object}   options.request
   * @param  {Object}   options.response
   * @param  {Function} next
   *
   * @return {void}
   */
  async handle({ request, response }, next) {
    if (['GET', 'HEAD', 'OPTIONS'].indexOf(request.method()) <= -1) {
      return next()
    }
    if (['OPTIONS'].indexOf(request.method()) != -1) {
      response.header("Access-Control-Allow-Origin", "*");
      response.header("Access-Control-Allow-Methods", "GET, HEAD, OPTIONS");
      response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      return response.send('ok')
    }
    let fileError = null
    for (let i = 0; i < staticServers.length; i++) {
      try {
        const server = staticServers[i];
        await server(request.request, request.response)
      } catch (error) {
        fileError = error
        if (error.status !== 404) {
          error.message = `${error.message} while resolving ${request.url()}`
          throw error
        }
      }
    }
    if (fileError && fileError.status === 404) {
      return next()
    }
  }
}

module.exports = function (Helpers, Config) {
  /**
   * Mount the static server if not already mounted
   */
  Logger.silly('Adding stetic server for', Config.get('http.staticFiles'))
  if (!(staticServers.length > Config.get('http.staticFiles').length)) {
    const options = Config.merge(Helpers.publicPath, defaultConfig)
    options.fallthrough = false
    Config.get('http.staticFiles').forEach(element => {
      staticServers.push(Helpers.promisify(serveStatic(element, options)))
    });
  }

  /**
   * Return middleware
   */
  return new Static()
}