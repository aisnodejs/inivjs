'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

module.exports = {
  cookie: require('./Cookie'),
  redis: require('./Redis'),
  file: require('./File')
}
