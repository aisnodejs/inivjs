'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

const _ = require('lodash')

module.exports = function (View) {
  View.global('old', function (key, defaultValue) {
    return _.get(this.ctx.flashMessages, key, defaultValue)
  })

  View.global('flashMessage', function (key, defaultValue) {
    return _.get(this.ctx.flashMessages, key, defaultValue)
  })
  View.global('hasFlashMessage', function (key) {
    return _.get(this.ctx.flashMessages, key, false)
  })

  View.global('errors', function () {
    return this.ctx.flashMessages.errors
  })

  View.global('hasErrors', function (key) {
    return !!_.size(this.ctx.flashMessages.errors)
  })

  View.global('getErrorFor', function (key) {
    const errors = this.ctx.flashMessages.errors

    /**
     * If errors is an object and not an array
     * then return the value for the key
     */
    if (_.isPlainObject(errors)) {
      return _.get(errors, key)
    }

    /**
     * Otherwise look inside array assuming validation
     * error structure
     */
    const errorMessage = _.find(errors, (error) => error.field === key || error.fieldName === key)
    return errorMessage ? errorMessage.message : null
  })

  View.global('hasErrorFor', function (key) {
    const errors = this.ctx.flashMessages.errors
    
        /**
         * If errors is an object and not an array
         * then return the value for the key
         */
        if (_.isPlainObject(errors)) {
          return _.get(errors, key)
        }
    
        /**
         * Otherwise look inside array assuming validation
         * error structure
         */
        const errorMessage = _.find(errors, (error) => error.field === key || error.fieldName === key)
        
    return (errorMessage) ? true : false
  })
}
