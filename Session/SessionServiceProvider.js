'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * SessionServiceProvider Class
 */
class SessionServiceProvider {
    constructor(iniv) {
        this._iniv = iniv
    }

    async register() {
        let self = this
        /*
		Register the http server to the system
		*/
        self._iniv._ioc.register('Iniv/Middleware/Session', () => {
            let Config = self._iniv._ioc.load('Iniv/Config');
            return new (require('./Middleware'))(Config);
        }).aliase('BodyParser')

        self._iniv._ioc.register('Iniv/Session', () => {
            return require('./Manager');
        }).aliase('Session')
        
        self._iniv._ioc.register('Iniv/Session', () => {
            return require('./Manager');
        }).aliase('Session')
        self._iniv._ioc.register('Iniv/Session/Client', () => {
            return require('./Client');
        })
        self._iniv._ioc.bind('Iniv/Traits/Session', () => {
            const Config = self._iniv._ioc.load('Iniv/Config')
            return ({ Request, Response }) => {
              require('./VowBindings/Request')(Request, Config)
              require('./VowBindings/Response')(Response, Config)
            }
          })
          return
    }
    async boot() {
        let self = this
        const HttpContext = self._iniv._ioc.load('Iniv/HttpContext')
        const WsContext = self._iniv._ioc.load('Iniv/WsContext')
        const Config = self._iniv._ioc.load('Iniv/Config')
        const SessionManager = self._iniv._ioc.load('Iniv/Session')
    
        /**
         * Adding getter to the HTTP context. Please note the session
         * store is not initialized yet and middleware must be
         * executed before the session store can be used
         * for fetching or setting data.
         */
        HttpContext.getter('session', function () {
          return require('./getRequestInstance')(this.request, this.response, Config, SessionManager)
        }, true)
        /**
         * Adding getter to the HTTP context. Please note the session
         * store is not initialized yet and middleware must be
         * executed before the session store can be used
         * for fetching or setting data.
         */
        WsContext.getter('session', function () {
            let session = require('./getRequestInstance')(this.request, this.response, Config, SessionManager);

            /**
             * Here we override methods on the session provider extended
             * class to make sure the end user is not mutating the
             * session state, since we do not have access to the
             * response object.
             * update : 19:03:18 now session commited on each event
             */
            /*['put', 'pull', 'flush', 'forget'].forEach((method) => {
                session[method] = function () {
                throw CE.RuntimeException.invalidAction('Cannot mutate session values during websocket request')
                }
            })
            */
            return session
          }, true)
        /**
         * Adding flash globals to the view layer, only when it is in use
         */
        try {
          require('./FlashGlobals')(self._iniv._ioc.load('Iniv/View'))
        } catch (error) {}
        return true
    }
}
module.exports = SessionServiceProvider