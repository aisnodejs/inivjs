'use strict'


module.exports = function getRequestInstance (request, response, Config, SessionManager) {
  const driver = Config.get('session.driver', 'cookie')
  Logger.silly('using %s driver', driver)

  const driverInstance = SessionManager.makeDriverInstance(driver)

  if (typeof (driverInstance.setRequest) === 'function') {
    driverInstance.setRequest(request, response)
  }

  const Session = require('./index')
  return new Session(request, response, driverInstance, Config)
}
