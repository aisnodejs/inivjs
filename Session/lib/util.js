'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

const ms = require('ms')
const util = exports = module.exports = {}

util.getCookieOption = function (Config, keyPrefix = null) {
  /**
   * Key to store session values
   *
   * @type {String}
   */
  const key = `${Config.get('session.cookieName', 'iniv-session')}`

  /**
   * Cookie options
   *
   * @type {Object}
   */
  const options = Config.merge('session.cookie', {
    httpOnly: true,
    sameSite: false
  })
  options.path = Config.get('session.path', '/')
  options.domain = Config.get('session.domain', null)
  options.secure = Config.get('session.secure', false)
  options.encrypt = Config.get('session.encrypt', false)
  /**
   * We need to set the expires option only when clearWithBrowser
   * is set to false.
   */
  if (!Config.get('session.clearWithBrowser', false)) {
    let age = Config.get('session.age', '2 hrs')
    age = typeof (age) === 'number' ? age : ms(age)
    options.expires = new Date(Date.now() + age)
  }
  return { options, key: keyPrefix ? `${key}-${keyPrefix}` : key }
}
