class Timer {
    constructor(Config, Helper) {
        this.Config = Config
        this.Helper = Helper
        this.activeTimers = [];
        this.removeInActiveTimers()
    }
    setTimeout (callback, interval, ...timerArgs) {
        if(!this.Config.get('app.trackTimers', false)){
            return setTimeout(callback, interval, ...timerArgs);
        }
        let self = this
        const handle = setTimeout(callback, interval, ...timerArgs);
        const index = self.activeTimers.indexOf(handle);
        if (index >= 0) {
            self.activeTimers.splice(index, 1);
        }
        return handle
    }
    setInterval (callback, interval, ...timerArgs) {
        if(!this.Config.get('app.trackTimers', false)){
            return setInterval(callback, interval, ...timerArgs);
        }
        let self = this
        const handle = setInterval(callback, interval, ...timerArgs);
        const index = self.activeTimers.indexOf(handle);
        if (index >= 0) {
            self.activeTimers.splice(index, 1);
        }
        self.activeTimers.push(handle);
        return handle
    }
    setImmediate (callback, interval, ...timerArgs) {
        if(!this.Config.get('app.trackTimers', false)){
            return setImmediate(callback, interval, ...timerArgs);
        }
        let self = this
        const handle = setImmediate(callback, interval, ...timerArgs);
        self.activeTimers.push(handle);
        return handle
    }
    getActiveTimers () {
        return this.activeTimers.slice();
    };
    removeInActiveTimers(){
        let self = this
        for (let i = self.activeTimers.length - 1; i >= 0; i--) {
            if(self.activeTimers[i]._called){
                self.activeTimers.splice(i, 1);
            }
        }
        setTimeout(this.removeInActiveTimers.bind(this), this.Config.get('app.removeInActiveTimer', 5000));
        return 
    }
}

module.exports = Timer