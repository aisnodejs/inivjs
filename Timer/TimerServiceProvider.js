class TimerServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
        /*
		Regster the logger to the system
		*/
		self._iniv._ioc.singleton('Iniv/Timer', () => {
            const Config = self._iniv._ioc.load('Iniv/Config')
            const Helper = self._iniv._ioc.load('Iniv/Helper')
			return new (require('./Timer'))(Config,Helper);
        }).aliase('Timer')
        return
    }
    async boot () {
        let self = this
        return true
    }
}
module.exports = TimerServiceProvider