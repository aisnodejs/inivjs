'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ValidationServiceProvider Class
 */
class ValidationServiceProvider {
    constructor(iniv) {
        this._iniv = iniv
    }
    async register() {
        let self = this
        /*
		Register the http server to the system
		*/
        self._iniv._ioc.bind('Iniv/Validator', () => {
            return require('./Validator');
        }).aliase('Validator')

        self._iniv._ioc.bind('Iniv/Middleware/Validator', () => {
            const MiddlewareValidator = require('./Middleware/Validator')
            return new MiddlewareValidator(self._iniv._ioc.load('Iniv/Validator'),self._iniv._ioc);
        })
        return
    }
    async boot() {
        let self = this
        const Server = self._iniv._ioc.load('Iniv/Server')
        Server.registerNamed({
            av: 'Iniv/Middleware/Validator'
        })

        const Router = self._iniv._ioc.load('Iniv/Router')
        Router.Route.macro('validator', function (validatorClass) {
            this.middleware([`av:${validatorClass}`])
            return this
        })
        Router.RouteResource.macro('validator', function (validatorsMap) {
            const middlewareMap = new Map()

            for (const [routeNames, validators] of validatorsMap) {
                const middleware = _.castArray(validators).map((validator) => `av:${validator}`)
                middlewareMap.set(routeNames, middleware)
            }

            this.middleware(middlewareMap)
            return this
        })
        return true
    }

}
module.exports = ValidationServiceProvider