'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

const indicative = require('../indicative')
const Validation = require('../Validation')
const { ValidationException } = require('../Exceptions')

module.exports = {
  validateAll: (...params) => new Validation(...params).runAll(),
  validate: (...params) => new Validation(...params).run(),
  sanitize: (...params) => indicative.sanitize(...params),
  rule: indicative.rule,
  is: indicative.is,
  sanitizor: indicative.sanitizor,
  formatters: indicative.formatters,
  extend: indicative.extend,
  ValidationException
}
