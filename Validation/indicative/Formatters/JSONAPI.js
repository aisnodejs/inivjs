'use strict'


/**
 * Formatter for JSONAPI
 *
 * @class JSONAPIFormatter
 */
class JSONAPIFormatter {
  constructor () {
    this.errors = []
  }

  /**
   * Stores the error
   *
   * @method addError
   *
   * @param  {Object} error
   */
  addError (error) {
    this.errors.push({
      title: error.validation,
      detail: error.message,
      source: {
        pointer: error.field
      }
    })
  }

  /**
   * Returns an array of errors
   *
   * @method toJSON
   *
   * @return {Array}
   */
  toJSON () {
    return {
      errors: this.errors
    }
  }
}

module.exports = JSONAPIFormatter
