'use strict'

/**
 * Mimimal formatter that returns the errors as it is.
 *
 * @class VanillaFormatter
 */
class VanillaFormatter {
  constructor () {
    this.errors = []
  }

  /**
   * Stores the error
   *
   * @method addError
   *
   * @param  {Object} error
   */
  addError (error) {
    this.errors.push(error)
  }

  /**
   * Returns an array of errors
   *
   * @method toJSON
   *
   * @return {Array}
   */
  toJSON () {
    return this.errors
  }
}

module.exports = VanillaFormatter
