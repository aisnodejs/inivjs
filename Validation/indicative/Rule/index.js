'use strict'

module.exports = function (name, args) {
  return {
    name,
    args: !args ? [] : (args instanceof Array === true ? args : [args])
  }
}
