'use strict'


const Validator = require('./Validator')
const Sanitization = require('./Sanitization')
const Formatters = require('./Formatters')
const Rule = require('./Rule')

Formatters.register('vanilla', require('./Formatters/Vanilla'))
Formatters.register('jsonapi', require('./Formatters/JSONAPI'))
Formatters.default('vanilla')

module.exports = {
  validate: Validator.validate,
  validateAll: Validator.validateAll,
  extend: Validator.extend,
  is: Validator.is,
  'is.extend': Validator.is.extend,
  sanitize: Sanitization.sanitize,
  sanitizor: Sanitization.sanitizor,
  rule: Rule,
  'sanitizor.extend': Sanitization.sanitizor.extend,
  formatters: Formatters
}
