const crypto = require('crypto')


class Loader {
    constructor(engine, Config, Helper) {
        this._engine = engine
        this.Config = Config
        this.Helper = Helper
        this._loaders = []
    }
    add(path) {
        if(this.Config.get('app.encryption', false)){
            this._loaders.push(new this.getLoaderForencryptedData(path))
        }else{
            this._loaders.push(new this._engine.FileSystemLoader(path))
        }
        return this
    }
    getLoaderForencryptedData() {
        return this._engine.Loader.extend({
            init: function (searchPaths, opts) {
                if (typeof opts === 'boolean') {
                    console.log(
                        '[nunjucks] Warning: you passed a boolean as the second ' +
                        'argument to FileSystemLoader, but it now takes an options ' +
                        'object. See http://mozilla.github.io/nunjucks/api.html#filesystemloader'
                    );
                }

                opts = opts || {};
                this.pathsToNames = {};
                this.noCache = !!opts.noCache;

                if (searchPaths) {
                    searchPaths = Array.isArray(searchPaths) ? searchPaths : [searchPaths];
                    // For windows, convert to forward slashes
                    this.searchPaths = searchPaths.map(self.Helper.path.normalize);
                }
                else {
                    this.searchPaths = ['.'];
                }

                if (opts.watch) {
                    // Watch all the templates in the paths and fire an event when
                    // they change
                    var chokidar = require('chokidar');
                    var paths = this.searchPaths.filter(fs.existsSync);
                    var watcher = chokidar.watch(paths);
                    var _this = this;
                    watcher.on('all', function (event, fullname) {
                        fullname = path.resolve(fullname);
                        if (event === 'change' && fullname in _this.pathsToNames) {
                            _this.emit('update', _this.pathsToNames[fullname]);
                        }
                    });
                    watcher.on('error', function (error) {
                        console.log('Watcher error: ' + error);
                    });
                }
            },

            getSource: function (name) {
                var fullpath = null;
                var paths = this.searchPaths;

                for (var i = 0; i < paths.length; i++) {
                    var basePath = self.Helper.path.resolve(paths[i]);
                    var p = self.Helper.path.resolve(paths[i], name);
                    if (p.indexOf(basePath) === 0 && self.Helper.fs.existsSync(p)) {
                        fullpath = p;
                        break;
                    }
                }

                if (!fullpath) {
                    return null;
                }
                this.pathsToNames[fullpath] = name;
                let data = self.Helper.fs.readFileSync(fullpath)
                let newBuffer = new Buffer(data.toString().replace(new RegExp('"', 'g'), ''), 'hex')
                let decipher = crypto.createDecipher('aes-256-cbc', self.Config.key);
                let dec = Buffer.concat([decipher.update(newBuffer), decipher.final()]);
                data = dec.toString('utf8')
                return {
                    src: data,
                    path: fullpath,
                    noCache: this.noCache
                };
            }
        });
    }
    getLoaders(){
        return this._loaders
    }
}

module.exports = Loader