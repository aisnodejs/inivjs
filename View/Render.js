const crypto = require('crypto')


class Render {
    constructor(engine, Config, Helper, Loader, globals, { request, response, session }) {
        this._engine = engine
        this.Config = Config
        this.Helper = Helper
        this.Loader = Loader
        this._globals = globals
        this._share = {}
        this._request = request
        this._response = response
        this._session = session
    }
    render(...params) {
        let self = this
        if (params[0].search('::') != -1) {
            params[0] = load(params[0].substr(0, params[0].search('::'))) + '/' + params[0].substr((params[0].search('::') + 2))
        } else {
            params[0] = load('Iniv/Helper').viewPath(params[0])
        }
        params[0] = params[0] + this.Config.get('views.ext', '.html');
        if (params[1]) {
            Object.assign(params[1], this._globals)
        } else {
            params[1] = this._globals
        }
        params[1].request = this._request
        params[1].response = this._response
        params[1].session = this._session
        Object.assign(params[1], this._share)
        this._response.header('Content-Type', 'text/html')
        try {
            let env = new this._engine.Environment(this.Loader.getLoaders(), { autoescape: true });
            return env.render(...params)
        } catch (e) {
            throw e
        }
    }
    share(...params) {
        let self = this
        params.forEach(function (arg) {
            Object.assign(self._share, arg)
        })
    }
}

module.exports = Render