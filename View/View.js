'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Response Class
 */
const nunjucks = require('nunjucks')
const Render = require('./Render')
const Loader = require('./Loader')
class View {
    constructor(Helper, Config) {
        this.Helper = Helper;
        this.Config = Config;
        this.engine = nunjucks
        this.loader = null
        this._globals = {
            siteName : this.Config.get('app.siteName','InivJs App'),
            version : this.Config.get('app.version','1.0.0'),
            Config : this.Config
        }
    }
    global(...params) {
        this._globals[params[0]] = params[1]
        return this
    }

    share(ctx) {
        return new Render(this.engine, this.Config, this.Helper, this.loader, this._globals,ctx)
    }

    addLoader(path){
        if(this.loader){
            this.loader.add(path)
        }else{
            this.loader = new Loader(this.engine, this.Config, this.Helper)
            this.loader.add(path)
        }
        return this
    }
}

module.exports = View