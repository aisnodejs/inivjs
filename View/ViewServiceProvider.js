'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * ViewServiceProvider Class
 */
class ViewServiceProvider {
    constructor (iniv) {
        this._iniv = iniv
    }
    async register () {
        let self = this
         /*
		Register the http server to the system
		*/
		self._iniv._ioc.singleton('Iniv/View', (request, View) => {
            let Helper = self._iniv._ioc.load('Iniv/Helper');
            let Config = self._iniv._ioc.load('Iniv/Config');
			return new (require('./View'))(Helper, Config);
        }).aliase('View')
        return
    }

    async boot () {
        const Context = this._iniv._ioc.load('Iniv/HttpContext')
        const View = this._iniv._ioc.load('Iniv/View')
        const Config = this._iniv._ioc.load('Iniv/Config')
        const Route = this._iniv._ioc.load('Iniv/Router')
        const Helpers = this._iniv._ioc.load('Iniv/Helper')
        const Localize = this._iniv._ioc.load('Iniv/Localize')
    
        /**
         * Registering wildely available globals
         */
        require('./globals')(View, Route, Config, Helpers, Localize)
        View.addLoader(Helpers.viewPath())
        View.addLoader(Helpers.appPath())
        View.addLoader(Helpers.rootPath())    
        /**
         * Registers an isolated instance of view on the
         * response object. Each view has access to
         * the request object.
         */
        Context.getter('view', function () {
          return View.share(this)
        }, true)
        return true
      }
}
module.exports = ViewServiceProvider