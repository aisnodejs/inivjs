'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

module.exports = function (View, Route, Config, Helper, Localize) {

  /**
   * Share helper class
   */
  View.global('appPath', (...args) => {return Helper.appPath(...args)})
  View.global('rootPath', (...args) => {Helper.rootPath(...args)})
  View.global('viewPath', (...args) => {
    return Helper.viewPath(...args)
  })
  View.global('translate', (...args) => {
    return Localize.translate(...args)
  })
  /**
   * Return url for the route
   */
  View.global('route', (...args) => Route.url(...args))

  /**
   * Make url for the assets file
   */
  View.global('assetsUrl', (url) => url && /^\/|^http(s)?/.test(url) ? url : `/${url}`)

  /**
   * Make link tag for css
   */
  View.global('css', function (url, skipPrefix = false) {
    url = !url.endsWith('.css') && !skipPrefix ? `${url}.css` : url
    return this.safe(`<link rel="stylesheet" href="${this.$globals.assetsUrl(url)}" />`)
  })

  /**
   * Make script tag for javascript
   */
  View.global('script', function (url, skipPrefix = false) {
    url = !url.endsWith('.js') && !skipPrefix ? `${url}.js` : url
    return this.safe(`<script type="text/javascript" src="${this.$globals.assetsUrl(url)}"></script>`)
  })
  /**
   * Make object to stringify
   */
  View.global('stringify', function (data = {}) {
    return JSON.stringify(data)
  })
  
}
