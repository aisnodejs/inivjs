'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * MIXIN: This is a mixin and has access to the Channel
 *        class instance
*/

const util = require('../../lib/util')
const co = require('co')

const LifeCycle = exports = module.exports = {}

/**
 * This method is invoked whenever a new socket joins a channel
 * and all middleware have been called. Here we initiate the
 * channel closure and pass around the socket and the
 * request to the constructor.
 *
 * @param  {Object} ws
 */
LifeCycle._onConnection = function (ws, events) {
  const self = this
  const closureInstance = []
  for (let i = 0; i < this._closure.length; i++) {
    closureInstance.push(util.isClass(this._closure[i]) ? (new this._closure[i](ws, this.presence)) : this._closure[i](ws, this.presence))
  }
  ws.socket.setParentContext(closureInstance)

  /**
   * Hooking into disconnected method and clearing off the socket
   * from the wsPool. Also calling the custom disconnected method.
   */
  ws.socket.on('disconnect', (resion) => this._onDisconnect(ws, resion))

  /**
   * Hooking into join:ad:room event and following a better
   * join room approach
   */
  ws.socket.on('join:ad:room', function* (payload, fn) {
    yield self._onJoinRoom(ws.socket, ws.request, payload, fn)
  })

  /**
   * Hooking into leave:ad:room event and following a better
   * leave room approach
   */
  ws.socket.on('leave:ad:room', function* (payload, fn) {
    yield self._onLeaveRoom(ws.socket, ws.request, payload, fn)
  })

  /**
   * If channel closure is a class, we need to bind all class
   * methods starting with {on} as the event listeners for
   * corresponding events.
   */
  for (let i = 0; i < this._closure.length; i++) {
    if (util.isClass(this._closure[i])) {
      const methods = Object.getOwnPropertyNames(this._closure[i].prototype)
      methods.forEach((method) => this._bindEventListener(ws, closureInstance[i], method))
    }
  }
  /**
   * If channel has on events, we need to bind all event listeners for corresponding events
   */
  for (let i = 0; i < events.length; i++) {
    events[i].callback = events[i].callback.bind(ws)
    this._bindEventListenerForCannelEvents(ws, events[i])
  }
}

/**
 * This method is invoked whenever a socket disconnects
 * and it also calls the disconnect method of the
 * channel if defined.
 *
 * @param  {Object} ws
 */
LifeCycle._onDisconnect = async function (ws, resion) {
  for (let i = 0; i < this._disconnectedFn.length; i++) {
    await this._disconnectedFn[i](ws, resion)
  }
  this._wsPool[ws.socket.id] = null
  delete this._wsPool[ws.socket.id]
  return
}

/**
 * Here we attach listeners for events defined as
 * event in channel.
 *
 * @param {Object} WsContext
 * @param {Object} event
 */
LifeCycle._bindEventListenerForCannelEvents = function (ws, event){
  ws.socket.on(event.eventName, async function (...arg) {
    Logger.silly('New event received :: ' + event.eventName)
    if (typeof arg[0] != 'function') {
      Logger.silly('data :: ', arg[0])
    }
    let composedFn = null
    let result = {}
    let action = null
    if (typeof arg[0] == 'function') {
      action = arg[0]
      arg[0] = {}
    }
    if (arg.length > 1) {
      action = arg[1]
    }
    let argument = {}
    try {
      if (load('Iniv/Config').get('ws.encrypt', false)) {
        argument = JSON.parse(load('Iniv/Ws/Crypto').decrypt(arg[0]))
      } else {
        argument = arg[0]
      }
      composedFn = load('Iniv/Ws').middleware.composeForEvent(load('Iniv/Ws').middleware.resolveForEvent(event.middlewares), { request: ws.request, socket: ws.socket, auth: ws.auth, data: argument })
    } catch (error) {
      Logger.error(error)
      result.status = 'fail'
      result.error = error.name
      result.message = error.message
      result.result = null
      result.statusCode = '500'
      if (action && typeof action == 'function') {
        return action(result)
      }
    }
    co(async function () {
      return await composedFn()
    }).then(async (data) => {
      if (!data) {
        try {
          data = await event.callback(argument)
        } catch (error) {
          Logger.error(error)
          result.status = 'fail'
          result.error = error.name
          result.message = error.message
          result.result = null
          result.statusCode = '500'
        }
      }
      if (data && Object.keys(data).indexOf('result') != -1) {
        result.result = data.result
      } else {
        result.result = data
      }
      if (data && Object.keys(data).indexOf('message') != -1) {
        result.message = data.message
      } else {
        result.message = 'Event process data successfully'
      }
      if (data && Object.keys(data).indexOf('status') != -1) {
        result.status = data.status
      } else {
        result.status = 'success'
      }
      if (data && Object.keys(data).indexOf('statusCode') != -1) {
        result.statusCode = data.statusCode
      } else {
        result.statusCode = '200'
      }
      if (ws.session) {
        await ws.session.commit()
      }
      Logger.silly('Event proccessed :: ' + event.eventName)
      if (action && typeof action == 'function') {
        if (load('Iniv/Config').get('ws.encrypt', false)) {
          return action(load('Iniv/Ws/Crypto').encrypt(JSON.stringify(result)))
        }
        return action(result)
      }
      return
    }).catch(async (error) => {
      Logger.error(error)
      result.status = 'fail'
      result.error = error.name
      result.message = error.message
      result.result = null
      result.statusCode = '500'
      if (ws.session) {
        await ws.session.commit()
      }
      Logger.silly('Event proccessed :: ' + util.generateEventName(method, true))
      if (action && typeof action == 'function') {
        return action(load('Iniv/Ws/Crypto').encrypt(JSON.stringify(result)))
      }
    })
  })
}
/**
 * Here we attach listeners for events defined as
 * function names on the socket class.
 *
 * @param {Object} closureInstance
 * @param {Object} socket
 * @param {String} method
 */
LifeCycle._bindEventListener = function (ws, closureInstance, method) {
  if (method.startsWith('on') && typeof (closureInstance[method]) === 'function') {
    let eventMiddleware = []
    if (closureInstance.register) {
      let middlewares = closureInstance.register()
      if (middlewares[method]) {
        eventMiddleware = Array.isArray(middlewares[method]) ? middlewares[method] : [middlewares[method]]
      }
    }
    let event = {
      eventName: util.generateEventName(method, true),
      middlewares: eventMiddleware,
      callback: closureInstance[method].bind(closureInstance)
    }
    this._bindEventListenerForCannelEvents(ws, event)
  }
}
