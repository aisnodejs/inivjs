'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * MIXIN: This is a mixin and has access to the channel
 *        class instance.
 */

const co = require('co')
const Socket = require('../../Socket')
const Response = require('../../Response')

const Setup = exports = module.exports = {}

/**
 * Initiating the socket by creating a new InivSocket
 * instance and saving it inside the socket pool.
 */
Setup._initiateSocket = function () {
  this.io.use((socket, next) => {
    const Context = this.Ioc.load('Iniv/WsContext')
    const ctx = new Context(this.io, socket, socket.request, new Response())
    this._wsPool[socket.id] = ctx
    next()
  })
}

/**
 * Calling custom middleware by looping over them and
 * throwing an error if any middleware throws error.
 */
Setup._callCustomMiddleware = function () {
  this.io.use((socket, next) => {
    const middlewareList = this.Middleware.resolve(this._middleware, true)
    if (!middlewareList.length) {
      next()
      return
    }
    const ctx = this.get(socket.id)
    const composedFn = this.Middleware.compose(middlewareList, ctx)
    co(async function () {
      await composedFn()
    })
    .then(() => next())
    .catch((error) => {
      next(error)
    })
  })
}
