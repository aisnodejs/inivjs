'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

const mixin = require('es6-class-mixin')
const _ = require('lodash')
const Mixins = require('./Mixins')
const CE = require('../Exceptions')
const Presence = require('../Presence')
const util = require('../lib/util')
const Resetable = require('resetable')

class Channel {
  constructor(io, name, closure, Config, Ioc, Middleware) {
    this.io = name === '/' ? io : io.of(name)
    this.presence = new Presence(this.io)
    this.Config = Config
    this.Ioc = Ioc
    this.Middleware = Middleware
    this._events = []
    /**
     * A reference to the closure, it will be executed after
     * all middleware method.
     *
     * @type {Function|Class}
     */
    this._closure = closure

    /**
     * A boolean to know whether closure is a class or not. When
     * closure is a class we call class methods for LifeCycle
     * events.
     *
     * @type {Boolean}
     */
    this._closureIsAClass = util.isClass(closure)


    /**
     * Custom middleware to be executed for each socket
     * connection.
     *
     * @type {Array}
     */
    this._middleware = []

    /**
     * Reference to the instances of Iniv Socket class
     * required to emit messages outside of the socket
     * loop.
     *
     * @type {Object}
     */
    this._wsPool = {}

    /**
     * Runtime variable to store the scope
     * of emit.
     *
     * @type {Array}
     */
    this._emitTo = new Resetable({
      ids: [],
      rooms: []
    })

    /**
     * The method to be invoked whenever someone leaves
     * the channel.
     *
     * @type {Function}
     */
    this._disconnectedFn = []
    for (let i = 0; i < this._closure.length; i++) {
      this._disconnectedFn.push(util.isClass(this._closure[i]) && this._closure[i].prototype.disconnected
        ? util.wrapIfGenerator(this._closure[i].prototype.disconnected.bind(this._closure[i]))
        : function () { })
    }

    /**
     * The method to be invoked whenever someone wants to
     * join a given room.
     *
     * @type {Function}
     */
    this._roomJoinFn = []
    for (let i = 0; i < this._closure.length; i++) {
      this._roomJoinFn.push(util.isClass(this._closure[i]) && this._closure[i].prototype.joinRoom
        ? util.wrapIfGenerator(this._closure[i].prototype.joinRoom)
        : function () { })
    }
    /**
     * The method to be invoked whenever someone wants to
     * leave a given room.
     *
     * @type {Function}
     */
    this._roomLeaveFn = []
    for (let i = 0; i < this._closure.length; i++) {
      this._roomJoinFn.push(util.isClass(this._closure[i]) && this._closure[i].prototype.leaveRoom
        ? util.wrapIfGenerator(this._closure[i].prototype.leaveRoom)
        : function () { })
    }
    /**
     * Lifecycle methods to be called as soon as
     * a channel is instantiated. Never change
     * the execution order of below methods
     */
    this._initiateSocket()
    this._callCustomMiddleware()

    /**
     * Hook into new connection and invoke the
     * channel closure.
     */
    this.io.on('connection', (socket) => {
      this._onConnection(this._wsPool[socket.id],this._events)
    })
  }
  /**
   * Add a event to the socket
   *
   * @param {...Spread} middleware
   */
  on(eventName, middlewares, callback){
    let event = {}
    if (typeof middlewares == 'string') {
      event.callback = callback
      event.middlewares = [middlewares]
      event.eventName = eventName
    }
    if (typeof middlewares == 'object' && Array.isArray(middlewares)) {
      event.callback = callback
      event.middlewares = middlewares
      event.eventName = eventName
    }
    if(typeof middlewares == 'function'){
      event.callback = middlewares
      event.middlewares = null
      event.eventName = eventName
    }
    if(Object.keys(event) == 0){
      throw new Error('EVENT_ARGUMENTS_NOT_VALID');
      
    }
    this._events.push(event)
    return this
  }
  /**
   * Add a middleware to the middleware stack
   *
   * @param {...Spread} middleware
   */
  middleware(middleware) {
    const args = _.isArray(middleware) ? middleware : _.toArray(arguments)
    this._middleware = this._middleware.concat(args)
    return this
  }

  /**
   * Send a message to a single room
   *
   * @param {String} room
   *
   * @return {Object} reference to {this} for chaining
   */
  inRoom(room) {
    const emitTo = this._emitTo.get()
    emitTo.rooms = [room]
    this._emitTo.set(emitTo)
    return this
  }

  /**
   * Send a message to multiple rooms
   *
   * @param {Array} rooms
   *
   * @return {Object} reference to {this} for chaining
   */
  inRooms(rooms) {
    const emitTo = this._emitTo.get()
    emitTo.rooms = rooms
    this._emitTo.set(emitTo)
    return this
  }

  /**
   * Emit messages to selected socket ids
   *
   * @param {Array} ids
   */
  to(ids) {
    const emitTo = this._emitTo.get()
    emitTo.ids = ids
    this._emitTo.set(emitTo)
    return this
  }

  /**
   * Emit an event to all sockets or selected sockets.
   */
  emit() {
    const emitTo = this._emitTo.pull()
    let args = _.toArray(arguments)
    if (load('Iniv/Config').get('ws.encrypt', false)) {
      args[1] = load('Iniv/Ws/Crypto').encrypt(JSON.stringify(args[1]))
    }
    if (_.size(emitTo.ids)) {
      emitTo.ids.forEach((id) => {
        const to = this.io.to(id)
        to.emit.apply(to, args)
      })
      return
    }

    if (_.size(emitTo.rooms)) {
      emitTo.rooms.forEach((room) => {
        const to = this.io.to(room)
        if (load('Iniv/Config').get('ws.sendRoomName', true)) {
          args.splice(1, 0, room)
        }
        to.emit.apply(to, args)
      })
      return
    }

    this.io.emit.apply(this.io, args)
  }

  /**
   * Returns socket instance using its id
   *
   * @param  {String} id
   *
   * @return {Object}
   */
  get(id) {
    return this._wsPool[id]
  }
  /**
   * Returns All socket instance 
   *
   *
   * @return {Object}
   */
  get sockets() {
    return this._wsPool
  }

  /**
   * Method to be disconnected everytime a new client
   * disconnects.
   *
   * @param {Function} fn
   */
  disconnected(fn) {
    if (typeof (fn) !== 'function') {
      throw CE.InvalidArgumentException.invalidParameter('Make sure to pass a function for disconnected event')
    }
    this._disconnectedFn.push(util.wrapIfGenerator(fn))
    return this
  }

  /**
   * The action to be executed whenever a user tries
   * to join a room. This is the best place make
   * sure only allowed users join the room.
   *
   * @return {Object} reference to this for chaining
   */
  joinRoom(fn) {
    if (typeof (fn) !== 'function') {
      throw CE.InvalidArgumentException.invalidParameter('Make sure to pass a function for joinRoom event')
    }
    this._roomJoinFn = util.wrapIfGenerator(fn)
    return this
  }

  /**
   * The action to be executed whenever a user tries
   * to join a room. This is the best place make
   * sure only allowed users join the room.
   *
   * @return {Object} reference to this for chaining
   */
  leaveRoom(fn) {
    if (typeof (fn) !== 'function') {
      throw CE.InvalidArgumentException.invalidParameter('Make sure to pass a function for leaveRoom event')
    }
    this._roomLeaveFn = util.wrapIfGenerator(fn)
    return this
  }
}

class ExtendedChannel extends mixin(
  Channel,
  Mixins.Setup,
  Mixins.LifeCycle,
  Mixins.Rooms
) { }

module.exports = ExtendedChannel
