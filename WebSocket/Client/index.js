'use strict'

const io = require('socket.io-client')
const WsManager = require('./Ws')
module.exports = new WsManager(io)
