'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * Crypto Class
 */
const crypto = require('crypto');

class Crypto {
	constructor(Config){
		this.Config = Config
		this.key = this.Config.get('ws.key')
		this.block = 16
		this.IvSize = 16
	}
	encrypt($value){
		if(!$value){return false;}
		if(typeof $value != 'string'){
			throw new Error('SOCKET_CRYPTO_BAD_ARGUMENT')
		}
		let padding = this.block - ($value.length % this.block);
		let iv =  crypto.randomBytes(this.IvSize)
		let cipher = crypto.createCipheriv('aes-128-cbc', this.key, iv);
		let encrypted = Buffer.concat([cipher.update($value, 'utf8'), cipher.final()]);
		return Buffer.concat([new Buffer(this.bytesToHex(iv),'utf8'), new Buffer(encrypted.toString('base64'),'utf8')]).toString('base64');
	}
	decrypt($value){
		if(!$value){return false;}
		if(typeof $value != 'string'){
			throw new Error('SOCKET_CRYPTO_BAD_ARGUMENT')
		}
		$value = new Buffer($value, 'base64');
		let iv = new Buffer(this.hexToBytes(($value.slice(0, 32)).toString('utf8')),'bytes');
		$value = $value.slice(32);
		$value = $value.toString('utf8')
		$value = new Buffer($value, 'base64')
		let decipher = crypto.createDecipheriv('aes-128-cbc', this.key, iv);
		decipher.setAutoPadding(false);
		let decrypted = decipher.update($value, 'binary', 'utf8') + decipher.final('utf8');
		return decrypted.replace(/\0/g, '');
	}
	hexToBytes(hex) {
		let bytes = []
		for (let i = 0; i < hex.length; i += 2){
			bytes.push(parseInt(hex.substr(i, 2), 16));
		}
		return bytes;
	}
	bytesToHex(bytes) {
		let hex = []
		for (let i = 0; i < bytes.length; i++) {
			hex.push((bytes[i] >>> 4).toString(16));
			hex.push((bytes[i] & 0xF).toString(16));
		}
		return hex.join("");
	}
}

module.exports = Crypto