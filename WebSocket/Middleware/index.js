'use strict'

/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

const CoCompose = require('co-compose')
const haye = require('haye')
const _ = require('lodash')
const CE = require('../Exceptions')

class Middleware {
  constructor(Config, Ioc) {
    this.store = new CoCompose()
    this.Config = Config
    this.Ioc = Ioc
  }


  /**
   * Returns the middleware store instance
   *
   * @return {Object}
   */
  getStore() {
    return this.store
  }

  /**
   * Register global middleware.
   *
   * @param  {Array} middlewareList
   */
  global(middlewareList) {
    this.store.tag('global').register(middlewareList)
  }

  /**
   * Return global middleware list
   *
   * @return {Array}
   */
  getGlobal() {
    return this.store.tag('global').get() || []
  }

  /**
   * Add named middleware
   *
   * @param  {Object} middlewareSet
   */
  named(middlewareSet) {
    this.store.tag('named').register([middlewareSet])
  }

  /**
   * Return named middleware.
   *
   * @return {Object}
   */
  getNamed() {
    return this.store.tag('named').get() ? this.store.tag('named').get()[0] : []
  }

  /**
   * Clear the middleware store by re-initiating the
   * store.
   */
  clear() {
    this.store = new CoCompose()
  }

  /**
   * Resolves named middleware and concats them with global middleware.
   * This is the final chain to be composed and executed.
   *
   * @param  {Array} namedList
   *
   * @return {Array}
   */
  resolve(namedList) {
    const namedMiddleware = this.getNamed()
    const globalMiddleware = this.getGlobal()
    return globalMiddleware.concat(_.map(namedList, (item) => {
      if (typeof (item) === 'function') {
        return { namespace: item, isFunction: true }
      }
      const formattedItem = haye.fromPipe(item).toArray()[0]
      if (!namedMiddleware[formattedItem.name]) {
        throw CE.RuntimeException.missingNamedMiddleware(formattedItem.name)
      }
      return { namespace: namedMiddleware[formattedItem.name], args: formattedItem.args }
    }))
  }

  /**
   * Resolves named middleware and concats them with global middleware.
   * This is the final chain to be composed and executed.
   *
   * @param  {Array} namedList
   *
   * @return {Array}
   */
  resolveForEvent(namedList) {
    const namedMiddleware = this.getNamed()
    const globalMiddleware = []
    return globalMiddleware.concat(_.map(namedList, (item) => {
      if (typeof (item) === 'function') {
        return { namespace: item, isFunction: true }
      }
      const formattedItem = haye.fromPipe(item).toArray()[0]
      if (!namedMiddleware[formattedItem.name]) {
        throw CE.RuntimeException.missingNamedMiddleware(formattedItem.name)
      }
      return { namespace: namedMiddleware[formattedItem.name], args: formattedItem.args }
    }))
  }
  /**
   * Compose middleware to be executed. Here we have all the custom
   * logic to call the middleware fn.
   *
   * @param  {Array} list
   * @param  {Object} socket
   * @param  {Object} request
   *
   * @return {function}
   */
  composeForEvent(list, ctx) {
    let self = this
    return this.store
      .runner(list)
      .resolve(function (middleware, params) {
        if (middleware.isFunction) {
          return middleware.namespace.apply(null, params)
        }
        const iocNamespace = middleware.namespace ? middleware.namespace : middleware
        const args = middleware.args || []
        let middlewareInstance = self.Ioc.resolveFunc(iocNamespace + '.handle')//load(iocNamespace)
        return middlewareInstance.method(...params.concat(args))
      })
      .withParams([ctx])
      .compose()
  }
  /**
   * Compose middleware to be executed. Here we have all the custom
   * logic to call the middleware fn.
   *
   * @param  {Array} list
   * @param  {Object} socket
   * @param  {Object} request
   *
   * @return {function}
   */
  compose(list, ctx) {
    return this.store
      .runner(list)
      .resolve(function (middleware, params) {
        if (middleware.isFunction) {
          return middleware.namespace.apply(null, params)
        }
        const iocNamespace = middleware.namespace ? middleware.namespace : middleware
        const args = middleware.args || []
        const middlewareInstance = load(iocNamespace)
        return middlewareInstance.handleWs.apply(middlewareInstance, params.concat(args))
      })
      .withParams([ctx])
      .compose()
  }
}


module.exports = Middleware

