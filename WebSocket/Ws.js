'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * WS Class
 */

const socketio = require('socket.io')
const Channel = require('./Channel')
const Middleware = require('./Middleware')
const CE = require('./Exceptions')

class Ws {
  constructor (Server, Helpers, Config, Ioc) {

    this.middleware = new Middleware(Config, Ioc)

    this.Config = Config
    this.io = null
    if (this.Config.get('ws', {}).useHttpServer) {
      this.attach(Server.getInstance())
    }

    this.Helpers = Helpers
    this.Ioc = Ioc
    this.controllersPath = 'Ws/Controllers'

    /**
     * Channels pool to store channel instances. This is done
     * to avoid multiple channel instantiation.
     *
     * @type {Object}
     */
    this._channelsPool = {}


  }

  /**
   * Returns a new/existing channel instance for
   * a given namespace.
   *
   * @param {String} name
   * @param {Function|Class} closure
   *
   * @return {Object} channelInstance
   *
   * @throws {Error} when trying to access a non-existing channel
   */
  channel (name, closure) {
    /**
     * If closure is a string. Resolve it as a controller from autoloaded
     * controllers.
     */
    if (typeof (closure) === 'string') {
      try {
        closure = [this.Ioc.load(this.Helpers.makeNameSpace(this.controllersPath, closure))]
      }catch(e){
        try{
          closure = [this.Ioc.load(closure)]
        }catch(e){
          closure = [this.Ioc.resolveSocketController(closure)]
        }
      }
    }
    /**
     * If closure is a string. Resolve it as a controller from autoloaded
     * controllers.
     */
    if (typeof (closure) === 'object' && Array.isArray(closure)) {
      for (let i = 0; i < closure.length; i++) {
        if (typeof (closure[i]) === 'string') {
          try {
            closure[i] = this.Ioc.load(this.Helpers.makeNameSpace(this.controllersPath, closure[i]))
          }catch(e){
            try{
              closure[i] = this.Ioc.load(closure[i])
            }catch(e){
              closure[i] = this.Ioc.resolveSocketController(closure[i])
            }
          }
        }
      }
    }
    if (typeof (closure) === 'function') {
      closure = [closure]
    }
    /**
     * Behave as a getter when closure is not defined.
     * Also make sure to throw exception when channel
     * has not been creating previously.
     */
    if (!closure) {
      const channel = this._channelsPool[name]
      if (!channel) {
        throw CE.RuntimeException.uninitializedChannel(name)
      }
      return channel
    }

    this._channelsPool[name] = this._channelsPool[name] || new Channel(this.io, name, closure, this.Config,this.Ioc, this.middleware)
    return this._channelsPool[name]
  }

  /**
   * Attach a custom http server. Make sure to call
   * attach before creating channels.
   *
   * @param {Object} server
   */
  attach (server) {
    this.io = socketio(server,{'pingInterval': this.Config.get('ws.pingInterval', 25000), 'pingTimeout': this.Config.get('ws.pingTimeout', 60000)})
    if (this.Config.get('ws', {}).useUws) {
      this.io.ws = new (require('uws').Server)({
        noServer: true,
        perMessageDeflate: false
      })
    }
  }

  /**
   * Register global middleware
   *
   * @param {Array} list
   */
  global (list) {
    this.middleware.global(list)
  }

  /**
   * Register named middleware
   *
   * @param {Object} set
   */
  named (set) {
    this.middleware.named(set)
  }
  loadKernel () {
    require(this.Helpers.socketKernel)
  }
  loadChanels () {
    this.Helpers.requireAll(this.Helpers.socketChannels)
  }
}


module.exports = Ws