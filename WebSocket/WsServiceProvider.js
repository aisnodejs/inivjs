'use strict'
/**
 * Copyright (c) Iniv Vinod Singh Rajput <vinodrajputsrcem@gmail.com>
 */

/**
 * WsServiceProvider Class
 */
class WsServiceProvider {
    constructor(iniv) {
        this._iniv = iniv
    }
    async register() {
        let self = this
        const Config = self._iniv._ioc.load('Iniv/Config')
        /*
		Register the http server to the system
        */
        self._iniv._ioc.singleton('Iniv/Ws', () => {
            const Server = self._iniv._ioc.load('Iniv/Server');
            const Helper = self._iniv._ioc.load('Iniv/Helper')
            return new (require('./Ws'))(Server, Helper, Config, self._iniv._ioc);
        }).aliase('Ws')

        self._iniv._ioc.singleton('Iniv/Ws/Crypto', () => {
            return new (require('./Crypto'))(Config);
        }).aliase('WsCrypto')

        self._iniv._ioc.bind('Iniv/Ws/Socket', () => {
            return require('./Socket')
        }).aliase('WsSocket')

        self._iniv._ioc.bind('Iniv/Ws/Response', () => {
            return require('./Response')
        }).aliase('WsSocket')

        // minify client
        if (Config.get('ws.buildClient')) {
            let config = {
                entry: self._iniv._ioc.load('Iniv/Helper').rootPath('/WebSocket/Client/index.js'),
                output: {
                    path: self._iniv._ioc.load('Iniv/Helper').resourcePath('/assets/js/'),
                    filename: 'ws.js',
                    libraryTarget: 'umd',
                    library: 'ws'
                },
                module: {
                    loaders: [{
                        test: /\.js$/,
                        exclude: /node_modules/,
                    }]
                }
            }
            const webpack = require('webpack')
            webpack(config, (err, stats) => {
                if (err || stats.hasErrors()) {
                }
            });
        }
        return
    }

    async boot() {
        let self = this
        self._iniv._ioc.load('Iniv/Ws').loadKernel()
        self._iniv._ioc.load('Iniv/Ws').loadChanels()
        return true
    }
}
module.exports = WsServiceProvider